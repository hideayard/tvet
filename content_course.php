<?php
$title = isset($_POST['title']) ? $_POST['title'] : ""; 

$table = "courses";

$query = "select * from $table where course_title like '%".strip_tags(str_replace('<br>', ' ', $title ) )."%'";
// echo $query;
$data_query = $db->rawQuery($query);
// var_dump($data_query);
?>
<?=$data_query[0]['course_content']?>
<div class="custom-breadcrumns border-bottom">
    <div class="container">
      <a href="index.php">Home</a>
      <span class="mx-3 icon-keyboard_arrow_right"></span>
      <a href="courses.html">Courses</a>
      <span class="mx-3 icon-keyboard_arrow_right"></span>
      <span class="current">Courses</span>
    </div>
  </div>


  <?php 
  if( isset($_SESSION['nama']) )  { 
      ?>
  <script>
  function actionEnroll()
  {
    let tdate = document.getElementById("tdate").value;
    let tdateText = $("#tdate option:selected").text();
    let token = document.getElementById("token").value;
    let course = 1;
        Swal.fire({
                title: 'Are you sure to Enroll This Course?',
                text: ""+tdateText,
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Enroll Now!',
                preConfirm: (login) => {
                    return fetch(`dashboard/actionenroll.php?session=`+tdate+`&course=`+course+`&token=`+token)
                    .then(response => {
                        if (!response.status) {
                        throw new Error(response.messages)
                        }
                        return response.json()
                    })
                    .catch(error => {
                        Swal.showValidationMessage(
                        `Request failed: ${messages}`
                        )
                    })
                },
                allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    console.log(result);
                if (result.isConfirmed || result.value) {
                    Swal.fire(
                    'Enrolled!',
                    'Thank you for enrolling for this course.<br>You can view the enrollment status in your dashboard.',
                    'success'
                    );

                }
                });
  }
  </script>
  <?php } 
  else
  { 
?>
<script>
  function actionEnroll()
  {


    Swal.fire({
                title: '<strong>User not found!</strong>',
                icon: 'info',
                html:
                    'You are not login / register to DaTA yet. Please login / register, ' + '',
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonText:
                    '<i class="fa fa-lock"></i> Login!',
                confirmButtonAriaLabel: 'Login!',
                cancelButtonText:
                    '<i class="fa fa-users"></i> Register',
                cancelButtonAriaLabel: 'Register!'
                }).then((result) => {
                    console.log(result);
                if (result.isConfirmed || result.value) {
                    window.location='login';
                }
                else if(result.dismiss == "cancel")
                {
                    window.location='register';
                }
                });

  }
  </script>
  <?php } ?>