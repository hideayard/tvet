<!-- <script src="../js/validation.min.js"></script> -->

<!-- jquery-validation -->
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>

<script>
 function isEmpty(obj) {
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                return false;
        }
          return true;
      }
// jQuery(document).ready(function() {
  $(document).ready(function () {
    $.validator.setDefaults({
    submitHandler: function () {
      console.log( "Form successful submitted!" );
      alert("submithandler");
    }
    });

    $('#dataform').validate({
        rules: {
                  course_title: {   required: true  }
                  ,course_desc: {   required: true  }         
                  ,course_content: {   required: true  }
                  ,course_type: {   required: true  }
                  ,course_price: {   required: true  }
                  // ,course_bil_peserta: {   required: true ,  number:true, }
        },
        messages: {
                  course_title: {  required: "Enter Title " }
                  ,course_desc: {  required: "Enter Desc " }
                  ,course_content: {  required: "Enter Content " }
                  ,course_type: {  required: "Enter Type"}
                  ,course_price: {  required: "Enter Price"}
        },

        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        },
        submitHandler: submitForm
        // submitHandler: function(form, event) { 
        //   event.preventDefault();
        //   alert("Do some stuff...");
        //   //submit via ajax
        // }
    });
   
    function isObject(obj) {
        return obj === Object(obj);
    }

function submitForm()
{
  $("#btnSubmit").html('<span class="fa fa-sync fa-spin"></span> Processing');
var form = $('#dataform')[0];
// Create an FormData object
var data = new FormData(form);
// console.log("data",data);
// console.log("form",form);
// disabled the submit button
$("#btnSubmit").prop("disabled", true);
      $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "action/course.php",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data) {
      try {
        console.log("0=",isObject(data),data);
        let rv = JSON.parse(data);
        console.log("1=");

        if(isEmpty(rv))
        {
                Swal.fire(
                'Info!',
                'No Data!',
                'info'
                );
            console.log("NO DATA : ", data);
            $("#btnLoadMore").html('Load More');
        }
        else
        {
          if(rv.status==true)
          {
            Swal.fire(
                'Success!',
                'Success Input Data!',
                'success'
                );
            console.log("SUCCESS : ", data);
            setTimeout(function(){ window.location="course"; }, 1000);
            // $('#my-awesome-dropzone')[0].dropzone.removeAllFiles(true); 
            $("#btnSubmit").html('<span class="fa fa-paper-plane"></span> Submit');
            // $("#dataform")[0].reset();

          }
          else 
          {
            Swal.fire(
                'error!',
                'Error Input Data, '+data,
                'error'
                );
            
            console.log("ERROR : ", data);
            $("#btnSubmit").html('<span class="fa fa-paper-plane"></span> Submit');

          }

        }
      } catch (e) {
        //error data not json
        Swal.fire(
                'error!',
                'Error Input Data, '+data,
                'error'
                );
            
            console.log("ERROR : ", data, e);
            $("#btnSubmit").html('<span class="fa fa-paper-plane"></span> Submit');
      } 

       
        $("#btnSubmit").prop("disabled", false);
        // $("#result").text(data);
        

    },
    error: function (e) {

        // $("#result").text(e.responseText);
        console.log("ERROR : ", e);
        $("#btnSubmit").prop("disabled", false);
        $("#btnSubmit").html('<span class="fa fa-paper-plane"></span> Submit');

    }
    }); //end of ajax
}

});
</script>
