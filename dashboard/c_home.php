
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.css">

 <style>

    /* Banner */

	#banner {
		/* background-color: #000000; */
		background-color: rgba(26, 25, 25, 0.97);
		background-image: url("dist/img/banner.jpg");
		background-size: cover;
		background-position: center center;
		background-repeat: no-repeat;
		color: #ffffff;
		padding: 6em 0;
		text-align: center;
		position: relative;
    height: 100%;
	}

		#banner:before {
			content: '';
			position: absolute;
			left: 0;
			top: 0;
			width: 100%;
			height: 100%;
			background: rgba(64, 72, 80, 0.55);
		}

		#banner .inner {
			color: #ffffff;

			position: relative;
			z-index: 1;
		}

			#banner .inner :last-child {
				margin-bottom: 0;
			}

		#banner h2, #banner h3, #banner h4, #banner h5, #banner h6 {
			color: #ffffff;
		}

		#banner .button.alt {
			box-shadow: inset 0 0 0 1px rgba(144, 144, 144, 0.75);
			color: #ffffff !important;
		}

			#banner .button.alt:hover {
				background-color: rgba(144, 144, 144, 0.275);
			}

			#banner .button.alt:active {
				background-color: rgba(144, 144, 144, 0.4);
			}

			#banner .button.alt.icon:before {
				color: #c1c1c1;
			}

		#banner .button {
			min-width: 12em;
		}

		#banner h2 {
			font-size: 3.5em;
			line-height: 1em;
			margin: 0 0 0.5em 0;
			padding: 0;
		}

		#banner p {
			font-size: 1.5em;
			margin-bottom: 2em;
		}

			#banner p a {
				color: #ffffff;
				text-decoration: none;
			}
    </style>