


<?php
$id_session = isset($_SESSION['i']) ? $_SESSION['i'] : "";

$maxfile = 1;
$filecount = 0;

$cid = isset($_POST['course_id']) ? $_POST['course_id'] : ""; 

if($cid!="") // && $tipe_user=="ADMIN")
{
  $sql = "SELECT * FROM courses WHERE course_id = '$cid' "; 
  $result = $db->rawQuery($sql);//@mysql_query($sql);
}
else if ($cid=="")
{
  $course_id = $id_session;
}

?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Courses</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Courses</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        
          <!-- /.col -->
          <div class="col-md-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <!-- <li class="nav-item"><a class="nav-link active" href="#view" data-toggle="tab">View</a></li> -->
                  <!-- <li class="nav-item"><a class="nav-link active" href="#update" data-toggle="tab">Update</a></li> -->
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  

                  <div class="active tab-pane" id="update">
                    <form class="form-horizontal" id="userform" action="#"  enctype="multipart/form-data" method="post">

                      <div class="form-group row">
                        <label for="course_name" class="col-sm-2 col-form-label">Title</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="course_title" name="course_title" placeholder="title" value="<?=$result[0]['course_title']?>">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="course_desc" class="col-sm-2 col-form-label">Description</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="course_desc" name="course_desc" placeholder="Description" value="<?=$result[0]['course_desc']?>">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="course_nama" class="col-sm-2 col-form-label">Content</label>
                        <div class="col-sm-10">
                          <!-- <div class="mb-3"> -->
                                <textarea id="course_content" name="course_content"  class="textarea" placeholder="Content"
                                        style="width: 100%; height: 400px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?=$result[0]['course_content']?></textarea>
                            <!-- </div> -->
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="course_type" class="col-sm-2 col-form-label">Type</label>
                        <div class="col-sm-10">
                        <input type="text" class="form-control" id="course_type" name="course_type" placeholder="Type" value="<?=$result[0]['course_type']?>">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="course_price" class="col-sm-2 col-form-label">PRICE</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="course_price" name="course_price" placeholder="Price" value="<?=$result[0]['course_price']?>">
                        </div>
                      </div>

                      <?php
                        //  if($tipe_user == "ADMIN") {
                                $arr_tipe_user = array("0" => "OFFLINE","1" => "ONLINE");
                      ?>
                      <div class="form-group row">
                        <label for="course_is_online" class="col-sm-2 col-form-label">IS ONLINE</label>
                        <div class="col-sm-10">
                        <select class="form-control select2bs4" id="course_is_online" name="course_is_online">
                          <?php
                          foreach ($arr_tipe_user as $key => $value)
                          {
                            $selected = " ";
                              if($result[0]['course_is_online'] == $key )
                              {
                                $selected = 'selected="selected"';
                              }
                              echo "<option value='".$key."' ".$selected ." >".$value."</option>" ;
                          }
                          ?>
                          </select>
                        </div>
                      </div>
                        <?php //} ?>  

                        <?php
                        //  if($tipe_user == "ADMIN") {
                                $arr_tipe_user = array("0" => "NONAKTIF","1" => "AKTIF");
                      ?>
                      <div class="form-group row">
                        <label for="course_status" class="col-sm-2 col-form-label">STATUS</label>
                        <div class="col-sm-10">
                        <select class="form-control select2bs4" id="course_status" name="course_status">
                          <?php
                          foreach ($arr_tipe_user as $key => $value)
                          {
                            $selected = " ";
                              if($result[0]['course_status'] == $key )
                              {
                                $selected = 'selected="selected"';
                              }
                              echo "<option value='".$key."' ".$selected ." >".$value."</option>" ;
                          }
                          ?>
                          </select>
                        </div>
                      </div>
                        <?php //} ?>  
                      
                    

                      
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit"  id="btnSubmit" name="btnSubmit" class="btn btn-primary">Submit</button>
                          <a href="home"><button type="button"  name="cancel" class="btn btn-secondary">Cancel</button></a>
                        </div>
                      </div>

                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  
<input type="hidden" id="maxfile" value="<?=$maxfile?>"/>
<input type="hidden" id="filecount" value="<?=$filecount?>"/>
<input type="hidden" id="filestatus1" value="1"/>

<script>

function hidden_div(filename,div_id)
{
  Swal.fire({
  title: 'Are you sure?',
  text: "Delete file ",
  html:
    '<p class="text text-danger">'+filename+'</p>',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {
    document.getElementById("preview"+div_id).style="display:none;";
 
 --document.getElementById("filecount").value;
 ++document.getElementById("maxfile").value;
 document.getElementById("filestatus"+div_id).value = 0;

 console.log(document.getElementById("filecount").value,document.getElementById("maxfile").value);
 //  $("div#my-awesome-dropzone").dropzone.options.maxFiles = 5;//document.getElementById("maxfile").value ;
    Swal.fire(
      'Deleted!',
      'Your file has been deleted.',
      'success'
    )
  }
})
 
}


Dropzone.autoDiscover = false;
	jQuery(document).ready(function() {

	  $("div#my-awesome-dropzone").dropzone({
            url: "#",
           // Prevents Dropzone from uploading dropped files immediately
           autoProcessQueue: false,
           addRemoveLinks: true,
           uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            maxFilesize: 5,
            acceptedFiles: "image/*",//,.doc,.docx,.xls,.xlsx,.csv,.tsv,.ppt,.pptx,.pages,.odt,.rtf",

          init: function() {
            var submitButton = document.querySelector("#btnSubmit")
                myDropzone = this; // closure

            submitButton.addEventListener("click", function() {
              myDropzone.processQueue(); // Tell Dropzone to process all queued files.
            });

            // You might want to show the submit button only when 
            // files are dropped here:
            this.on("addedfile", function(file) {
              // let str = JSON.stringify(file);

              console.log("addedfile",file);
              var ext = file.name.split('.').pop();

            if (ext == "pdf") {
                $(file.previewElement).find(".dz-image img").attr("src", "dist/img/pdf.png");
            } else if (ext.indexOf("doc") != -1) {
                $(file.previewElement).find(".dz-image img").attr("src", "dist/img/word.png");
            } else if (ext.indexOf("xls") != -1) {
                $(file.previewElement).find(".dz-image img").attr("src", "dist/img/excel.png");
            }
              // Show submit button here and/or inform user to click it.
            });
            this.on("error", function(file){if (!file.accepted) this.removeFile(file);});


          }
	  });

  });


   $(document).ready(function () {

$.validator.setDefaults({
submitHandler: function () {
  console.log( "Form successful submitted!" );
}
});

$('#userform').validate({
    rules: {
              course_nama: {   required: true,  }
              ,course_email: {   required: true, email: true, }         
              ,course_name: {   required: true  }
              // ,course_tempat: {   required: true,  }
              // ,course_agency: {   required: true,  }
              // ,course_program: {   required: true,  }
              // ,course_bil_peserta: {   required: true ,  number:true, }
    },
    messages: {
              course_nama: {  required: "Enter Name ", }
              ,course_email: {  required: "Enter Email " ,}
              ,course_name: {  required: "Enter Username ", }
              // ,course_tempat: {  required: "Enter Place First" ,}
              // ,course_agency: {  required: "Enter Agency First", }
              // ,course_program: {  required: "Enter Program First", }
              // ,course_bil_peserta: {  required: "Enter Participant First",}
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    },
    submitHandler: submitForm
  });

function submitForm()
{
  $("#btnSubmit").html('<span class="fa fa-sync fa-spin"></span> Processing');
var form = $('#userform')[0];
// Create an FormData object
var data = new FormData(form);
if(!isEmpty($('#my-awesome-dropzone')[0].dropzone.getAcceptedFiles()[0]))
  {
    data.append("course_foto", $('#my-awesome-dropzone')[0].dropzone.getAcceptedFiles()[0]);
  }
  data.append("course_id","<?=$course_id?>");
// disabled the submit button
$("#btnSubmit").prop("disabled", true);
      $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "actionuser.php",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data) {
      var rv;
      try {
        rv = JSON.parse(data);
        if(isEmpty(rv))
        {
                Swal.fire(
                'Info!',
                'No Data!',
                'info'
                );
            console.log("NO DATA : ", data);
            $("#btnLoadMore").html('Load More');
        }
        else
        {
          if(rv.status==true)
          {
            Swal.fire(
                'Success!',
                'Success Input Data!',
                'success'
                );
            console.log("SUCCESS : ", data);
            // setTimeout(function(){ window.location="users"; }, 1000);
            // $('#my-awesome-dropzone')[0].dropzone.removeAllFiles(true); 
            $("#btnSubmit").html('<span class="fa fa-paper-plane"></span> Submit');
            // $("#userform")[0].reset();

          }
          else 
          {
            Swal.fire(
                'error!',
                'Error Input Data, '+data,
                'error'
                );
            
            console.log("ERROR : ", data);
            $("#btnSubmit").html('<span class="fa fa-paper-plane"></span> Submit');

          }

        }
      } catch (e) {
        //error data not json
        Swal.fire(
                'error!',
                'Error Input Data, '+data,
                'error'
                );
            
            console.log("ERROR : ", data);
            $("#btnSave").html('<span class="fa fa-save"></span> Save');
      } 

       
        $("#btnSubmit").prop("disabled", false);
        // $("#result").text(data);
        

    },
    error: function (e) {

        // $("#result").text(e.responseText);
        console.log("ERROR : ", e);
        $("#btnSubmit").prop("disabled", false);
        $("#btnSubmit").html('<span class="fa fa-paper-plane"></span> Submit');

    }
    }); //end of ajax
}

});
</script>
