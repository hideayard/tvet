


<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {

    var tabel = $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "processing": true,
        "serverSide": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
            <?php
            echo '"ajax": "get_data_course.php?id='.$id.'&mode=list"';echo ',"order": [[ 1, "desc" ]],'; 
            ?>
            
        }); //end of datatables
  });
</script>

