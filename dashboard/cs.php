<div class="row">
    <div class="col-lg-12">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><a href="home">Dashboard</a></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Courses Catalog</li>
                    </ol>
                </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
            <div class="container-fluid">
           
                <!-- Main row -->
                <div class="row">
                <!-- Left col -->
                <div class="col-md-8">
                <div class="card">
              <div class="card-header">
                <h3 class="card-title">Courses Catalog</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                </button>
                </div>
            </div> <br>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Category</th>
                      <th>Task</th>
                      <th>Rating</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1.</td>
                      <td>Programming <br></td>
                      <td>Primitive Data Type</td>
                      <td><i class="fas fa-star text-warning"><i class="fas fa-star text-warning"><i class="fas fa-star text-warning"><i class="fas fa-star text-secondary"><i class="fas fa-star text-secondary"></i></td>
                      <td><button onclick="enrollnow(1);" class="btn btn-info"><span>Enroll Now</span></button></td>
                    </tr>
                    <tr>
                      <td>2.</td>
                      <td>Electronics</td>
                      <td>Transistor - Concept of Transistor</td>
                      <td><i class="fas fa-star text-warning"><i class="fas fa-star text-warning"><i class="fas fa-star text-warning"><i class="fas fa-star text-warning"><i class="fas fa-star text-secondary"></i></td>
                      <td><span class="badge bg-success">You have enrolled this course</span></td>
                    </tr>
                    <tr>
                      <td>3.</td>
                      <td>Electronics</td>
                      <td>Transistor - NPN and PNP transistor</td>
                      <td><i class="fas fa-star text-warning"><i class="fas fa-star text-warning"><i class="fas fa-star text-warning"><i class="fas fa-star text-secondary"><i class="fas fa-star text-secondary"></i></td>
                      <td><button onclick="enrollnow(2);" class="btn btn-info"><span>Enroll Now</span></button></td>
                    </tr>
                    <tr>
                      <td>4.</td>
                      <td>Arduino</td>
                      <td>I/O Pins Arduino</td>
                      <td><i class="fas fa-star text-warning"><i class="fas fa-star text-warning"><i class="fas fa-star text-warning"><i class="fas fa-star text-warning"><i class="fas fa-star text-warning"></i></td>
                      <td><button onclick="enrollnow(3);" class="btn btn-info"><span>Enroll Now</span></button></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div>
            </div>
                   
                </div>
                <!-- /.col -->

                <div class="col-md-4">
                    

                    <!-- PRODUCT LIST -->
                    <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Category</h3>

                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <ul class="products-list product-list-in-card pl-2 pr-2">
                        <li class="item">
                        Health and Safety (40)
                        </li>
                        <li class="item">
                        Programming (55)
                        </li>
                        <li class="item">
                        Electronics (25)
                        </li>
                        <li class="item">
                        Game Development (23)
                        </li>
                       
                        </ul>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer text-center">
                        <a href="javascript:void(0)" class="uppercase">View All Category</a>
                    </div>
                    <!-- /.card-footer -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div><!--/. container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
  </div>
</div>