<h3>Course Survey</h3>
                                <div style="text-align:left;">
                                <p class="MsoNormal"><span lang="EN-US">1. What do you think about the content of this course
                                    <br>
                                    <div class="form-group">
                                    <label>
                                        <input type="radio" name="optionsRadiosSurvey1" id="survey1A" value="A" checked="">
                                        <span class="text"> 
                                        Extremely Satisfied
                                        </span>
                                    </label>
                                    
                                    <label>
                                        <input type="radio" name="optionsRadiosSurvey1" id="survey1B" value="B" checked="">
                                        <span class="text"> 
                                        Satisfied
                                        </span>
                                    </label>

                                    <label>
                                        <input type="radio" name="optionsRadiosSurvey1" id="survey1C" value="C" checked="">
                                        <span class="text"> 
                                        Not Sure
                                        </span>
                                    </label>

                                    <label>
                                        <input type="radio" name="optionsRadiosSurvey1" id="survey1D" value="D" checked="">
                                        <span class="text"> 
                                        Not Satisfied
                                        </span>
                                    </label>

                                    <label>
                                        <input type="radio" name="optionsRadiosSurvey1" id="survey1E" value="E" checked="">
                                        <span class="text"> 
                                        Extremely Not Satisfied
                                        </span>
                                    </label>
                                    
                                        
                                    </div>
                                   
                                    <br>
                                    2. What do you think about the interactivity of this course?<br>
                                    pendawaian baru atau pertambahan pendawaian yang ada pada mana-mana premis,<br>
                                    terlebih dahulu mendapat kelulusan bertulis daripada ...<br>
                                    <div class="form-group">
                                    <label>
                                        <input type="radio" name="optionsRadiosSurvey2" id="survey2A" value="A" checked="">
                                        <span class="text"> 
                                        Extremely Satisfied
                                        </span>
                                    </label>
                                    
                                    <label>
                                        <input type="radio" name="optionsRadiosSurvey2" id="survey2B" value="B" checked="">
                                        <span class="text"> 
                                        Satisfied
                                        </span>
                                    </label>

                                    <label>
                                        <input type="radio" name="optionsRadiosSurvey2" id="survey2C" value="C" checked="">
                                        <span class="text"> 
                                        Not Sure
                                        </span>
                                    </label>

                                    <label>
                                        <input type="radio" name="optionsRadiosSurvey2" id="survey2D" value="D" checked="">
                                        <span class="text"> 
                                        Not Satisfied
                                        </span>
                                    </label>

                                    <label>
                                        <input type="radio" name="optionsRadiosSurvey2" id="survey2E" value="E" checked="">
                                        <span class="text"> 
                                        Extremely Not Satisfied
                                        </span>
                                    </label>

                                    
                                    </div>
                                </span></p>
                                </div>
                                    <br>
                                    <br>
                                    <button class="btn btn-info btn-submit">Submit Survey</button>