<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    session_start();
    $tipe = isset($_SESSION['t']) ? $_SESSION['t'] : "";
    $id_user = isset($_SESSION['i']) ? $_SESSION['i'] : "";
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
include_once ("config/db.php");

// DB table to use
$table = 'courses';
 
// Table's primary key
$primaryKey = 'course_id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$counter=0;
$cid=0;

$id = isset($_GET['id']) ? $_GET['id'] : ""; 
$i=-1;
$columns = array(
    array(
        'db'        => 'course_id',
        'dt'        => ++$i,
        'formatter' => function( $d, $row ) {
            global $counter;
            global $cid; 
            $cid = $d;
            return $counter++;
        }
    )
    ,array(
        'db'        => 'course_id',
        'dt'        => ++$i,
        'formatter' => function( $d, $row ) {
                    $tipe = isset($_SESSION['t']) ? $_SESSION['t'] : "";
                    $params = json_encode( array("course_id" => $d) );
                    if($tipe == "ADMIN")
                    {
                        return '<a onclick=\'postAndRedirect("detail_course",'.$params.')\' class="btn btn-primary"><span><i class="fa fa-eye"></i></span></a> | <a onclick="actiondelete(\'user\',\'user\','.$d.')" class="btn btn-danger"><span><i class="fa fa-trash"></i></span></a>' ;
                    }
                    else
                    {
                        return '<a onclick=\'postAndRedirect("detail_course",'.$params.')\' class="btn btn-primary"><span><i class="fa fa-eye"></i></span></a> ';
                    }


        }
    ),
    array( 'db' => 'course_title', 'dt' => ++$i )
    ,array(
        'db'        => 'course_desc',
        'dt'        => ++$i,
        'formatter' => function( $d, $row ) {
            global $cid; 
            $words = explode(" ", $d);
            $tenwords = array_slice($words,0,20);
            if(count($tenwords)>=20)
            {
                $String_of_ten_words = implode(" ",$tenwords)."\n"."... <a href='#'>Read More.</a>";
            }
            else
            {
                $String_of_ten_words = implode(" ",$tenwords)."\n";
            }
            return $String_of_ten_words;
            // return substr($d, 0, 200)."... Read More.";
        }
    )
    ,array(
        'db'        => 'course_content',
        'dt'        => ++$i,
        'formatter' => function( $d, $row ) {
            global $cid; 
            $params = json_encode( array("course_id" => $cid) );

            $words = explode(" ", $d);
            $tenwords = array_slice($words,0,20);
            if(count($tenwords)>=20)
            {
                $String_of_ten_words = implode(" ",$tenwords)."\n"."... <a ".'onclick=\'postAndRedirect("detail_course",'.$params.')\''.">Read More.</a>";
            }
            else
            {
                $String_of_ten_words = implode(" ",$tenwords)."\n";
            }
            return $String_of_ten_words;
            // return substr($d, 0, 200)."... Read More.";
        }
    ),
    // array( 'db' => 'course_desc',  'dt' => ++$i ),
    // array( 'db' => 'course_content',   'dt' => ++$i ),
    array( 'db' => 'course_type',   'dt' => ++$i ),
    array( 'db' => 'course_price',   'dt' => ++$i ),
    array( 'db' => 'course_status',   'dt' => ++$i )
  
);
 

// var_dump($columns) ;
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require( 'ssp.class.php' );
// var_dump (SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns,null, " course_status = 1" ));
echo json_encode(
    // SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
    SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns,null, " course_is_deleted = 0" )
);