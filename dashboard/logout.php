<?php
if(!isset($_SESSION)) 
{ 
	session_start(); 
} 
	unset($_SESSION['i']);
	unset($_SESSION['u']);
	unset($_SESSION['e']);
	unset($_SESSION['t']);
	
	if(session_destroy())
	{
        // header("Location: login.php");
        // set the expiration date to one hour ago
        setcookie("token", "", time() - 3600);
        setcookie("user", "", time() - 3600);
        setcookie("nama", "", time() - 3600);
		// exit(); //hentikan eksekusi kode di login_proses.php
		echo "<script>window.location='home';</script>";
	}
?>