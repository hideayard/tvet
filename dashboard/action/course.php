<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
// error_reporting(0);
// session_start();
// echo 1;


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
date_default_timezone_set('Asia/Singapore');

require_once ('../config/MysqliDb.php');
include_once ("../config/db.php");
$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
include("../config/functions.php");    

$id_session = isset($_SESSION['i']) ? $_SESSION['i'] : "";
$tipe_session = isset($_SESSION['t']) ? $_SESSION['t'] : "";

$target_dir = "uploads/user/";
$mode = isset($_POST['mode']) ? $_POST['mode'] : ""; 
$type = isset($_POST['type']) ? $_POST['type'] : ""; 
$tgl = (new \DateTime())->format('Y-m-d H:i:s');
// switch($mode)
// {
//   case "submit" : {$course_status = 1;}break;
//   case "save" : {$course_status = 2;}break;
//   // case "delete" : {$course_status = 3;}break;
//   default : {$course_status = 0;}break;
// }
//$txt_field= "course_title,course_desc,course_content,course_type,course_price,course_status";

            $course_id = isset($_POST['course_id']) ? $_POST['course_id'] : ""; 
            $course_title = isset($_POST['course_title']) ? $_POST['course_title'] : ""; 
            $course_desc = isset($_POST['course_desc']) ? $_POST['course_desc'] : ""; 
            $course_content = isset($_POST['course_content']) ? $_POST['course_content'] : ""; 
            $course_status = isset($_POST['course_status']) ? $_POST['course_status'] : ""; 
            $course_type = isset($_POST['course_type']) ? $_POST['course_type'] : ""; 
            $course_price = isset($_POST['course_price']) ? $_POST['course_price'] : ""; 

          $message = "Insert Sukses!!";
          $tgl = (new \DateTime())->format('Y-m-d H:i:s');
          
       

  $data = Array (  );
  if($course_title!="")
  {
      $data += array('course_title' => $course_title);
  }
  if($course_desc!="")
  {
      $data += array('course_desc' => $course_desc);
  }
  if($course_content!="")
  {
      $data += array('course_content' => $course_content);
  }
  if($course_type!="")
  {
      $data += array('course_type' => $course_type);
  }
  if($course_price!="")
  {
      $data += array('course_price' => $course_price);
  }

  if($course_status!="")
  {
      $data += array('course_status' => $course_status);
  }


// var_dump($data);
  $hasil_eksekusi = false;

if(isset($_POST['course_id']))
{    
  if($mode == "delete" && $tipe_session=="ADMIN")
    {
      $db->where('course_id', $course_id);
      $hasil_eksekusi = $db->delete('users');
      $message = "Delete Success !!";
    }
    else
    {
      $data += array('course_modified_by' => $id_session);
      $data += array('course_modified_at' => $tgl);
      $data += array('course_is_deleted' => 0);
      if( $tipe_session=="ADMIN")
      {
        $db->where ('course_id', $course_id);
      }
      else
      {
        $db->where ('course_id', $id_session);
      }
      // var_dump($data);
      $hasil_eksekusi = $db->update ('courses', $data);
      $message = "Update Success !!";

    }
    
    if ($hasil_eksekusi)
    {   
      echo json_encode( array("status" => true,"info" => $course_status,"messages" => $message ) );
    }//$db->count . ' records were updated';
    else
    {   
      echo json_encode( array("status" => false,"info" => 'update failed: ' . $db->getLastError(),"messages" => $message ) );

    }

  }
  else
  {  
    $message = "Insert Success";
      $data += array("course_id" => null);
      $data += array('course_created_by' => $id_session);
      $data += array('course_created_at' => $tgl);
    if($db->insert ('courses', $data))
    {
      echo json_encode( array("status" => true,"info" => $course_status,"messages" => $message ) );
  
      // $message = 1;//"Insert berhasil!";
    }
    else
    {
      $message = "Insert Fail";
      // echo 0;
      echo json_encode( array("status" => false,"info" => $db->getLastError(),"messages" => $message ) );
  
  
    }
  }
  

?>