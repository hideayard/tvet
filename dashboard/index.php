<?php
session_start();
date_default_timezone_set('Asia/Singapore');
$page = isset($_GET['page']) ? $_GET['page'] : "home"; 

error_reporting(0);
session_start();
require_once ('config/MysqliDb.php');
include_once ("config/db.php");
include_once ("config/functions.php");

$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
$tgl = (new \DateTime())->format('Y-m-d H:i:s');          
$id = isset($_SESSION['i']) ? $_SESSION['i'] : ""; 
$nama = isset($_SESSION['nama']) ? $_SESSION['nama'] : ""; 
$email=isset($_SESSION['e']) ? $_SESSION['e'] : ""; 
$tipe_user=isset($_SESSION['t']) ? $_SESSION['t'] : ""; 
$foto=isset($_SESSION['f']) ? $_SESSION['f'] : ""; 

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Digital TVET Academy (DATA)</title>
  <?php if( !isset($_SESSION['nama']) )  { ?>
          <script>
              alert("please login first");
              window.location ="../";
          </script>   
                 
  <?php } ?>
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2/sweetalert2.min.css">
  <?php
  if (file_exists("c_".$page.".php")) 
  {
    include_once ("c_".$page.".php");
  }
  ?>
 

<?php
    ////////////////////////////////////
$cookie_name = "user";
if(!isset($_COOKIE[$cookie_name]) || $_COOKIE[$cookie_name] == "") {
  echo "Cookie named '" . $cookie_name . "' is not set!";
  ?>
  <script>
  alert("token not found");
  window.location ="../";
  </script>
  <?php
  header("../index.php");die;
} 
else {
  // echo "Cookie '" . $cookie_name . "' is set!<br>";
  // echo "Value is: " . $_COOKIE[$cookie_name];
  ?>
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
  <?php
}

?>
</head>
<body class="hold-transition layout-top-nav layout-navbar-fixed sidebar-collapse">

<div class="wrapper">
<?php 
include_once ("navbar_admin.php"); 



// include_once ("content_home.php"); 
if($page == "" || $page == "home")
{ 
  // echo "1<hr>1<hr>11<hr>11<hr>11<hr>11<hr>11111111111";
  include_once ("content_home.php");
}
else
{ 
  // echo "2<hr>2<hr>2<hr>2<hr>2<hr>22<hr>11111111111";
  if (file_exists("".$page.".php")) 
    {
      include_once ("".$page.".php");
    }
    else 
    {
      include_once ("error.php");
    }
}

// include_once ("sidebar_admin.php"); 
// include_once ("control_sidebar.php"); 


?>


</div>
<?php 
include_once ("sidebar.php"); 
include_once ("footer.php"); 

include_once ("modals.php"); 

?>

<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>

<?php
  if (file_exists("s_".$page.".php")) 
  {
    include_once ("s_".$page.".php");
  }
?>  

<script>
 function isEmpty(obj) {
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                return false;
        }
          return true;
      }
 function postAndRedirect(url,postData)
{
  console.log(postData);
    var postFormStr = "<form method='POST' action='" + url + "'>\n";
    var obj = postData;

      for (var key in postData)
      {
          if (postData.hasOwnProperty(key))
          {
              postFormStr += "<input type='hidden' name='" + key + "' value='" + postData[key] + "'></input>";
          }
      }
      console.log(postFormStr);

    postFormStr += "</form>";

    var formElement = $(postFormStr);

    $('body').append(formElement);
    $(formElement).submit();
}

function actiondelete(module,type,module_id)
{
 
  Swal.fire({
  title: 'Are you sure?',
  // text: "Delete Data ",
  // html: '<p class="text text-danger">'+filename+'</p>',
  // icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {
    var data = new FormData();
    data.append("mode", "delete");//module);
    data.append("type", type);
    data.append(module+"_id", module_id);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "action"+module+".php",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data) {
          var rv;
          try {
            rv = JSON.parse(data);
            if(isEmpty(rv))
            {
                    Swal.fire(
                    'Info!',
                    'No Data!',
                    'info'
                    );
                console.log("NO DATA : ", data);
                $("#btnLoadMore").html('Load More');
            }
            else
            {
              if(rv.status==true)
              {
                Swal.fire(
                    'Success!',
                    'Success Delete Data!',
                    'success'
                    );
                console.log("SUCCESS Delete: ", data);
               setTimeout(function(){ location.reload();
                //  $('#example2').DataTable().ajax.reload();
                 }, 1000);
              }
              else if(rv.info==1 || rv.info==2)
              {
                Swal.fire(
                      'Success!',
                      'Success Delete Data!',
                      'success'
                      );
                console.log("SUCCESS DELETE : ", data);
                setTimeout(function(){ location.reload();
                //  $('#example2').DataTable().ajax.reload();
                 }, 1000);              }

            }
          } catch (e) {
            //error data not json
            Swal.fire(
                    'error!',
                    'Error Delete Data, '+data,
                    'error'
                    );
                
                console.log("ERROR : ", data);
          } 
        },
        error: function (e) {
            console.log("ERROR : ", e);
        }
    });
//  console.log(document.getElementById("filecount").value,document.getElementById("maxfile").value);
 //  $("div#my-awesome-dropzone").dropzone.options.maxFiles = 5;//document.getElementById("maxfile").value ;
    
  }
})
 
}

</script>

</body>
</html>
