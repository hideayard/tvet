<?php
date_default_timezone_set('Asia/Singapore');
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
error_reporting(0);
session_start();
require_once ('config/MysqliDb.php');
include_once ("config/db.php");
$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
$db2 = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);

include("config/functions.php");  
require_once("tokenlogin.php");

$secret = "B15m1ll4#";

$file = basename($_SERVER['PHP_SELF']);
$filename = (explode(".",$file))[0];
$tgl = (new \DateTime())->format('Y-m-d H:i:s');          

{
    $token = isset($_GET['token']) ? $_GET['token'] : ""; 
    // $jsondata = isset($_GET['jsondata']) ? $_GET['jsondata'] : ""; 
    // $data = json_decode($jsondata);
    $status = false;
    $msg = "Please Input Token!";
    if (json_last_error() === JSON_ERROR_NONE) {
        $status = true;
        $msg = "JSON OK";
        //do something with $json. It's ready to use
    } else {
        $status = false;
        $msg = "JSON ERROR";
    }

    $otl = new TokenLogin($secret);
    if($token!="")
    {
        try {
            $payload = $otl->validate_token($token);
    
        if ($payload) {
                $status = true;
                $msg =  "Valid token!";// You are user #{$payload->uid}";
                // $hasil = $payload;
                //action save to DB when token valid
            } else {
                $status = false;
                $msg =  "Invalid token";
            }
        } catch (Exception $e) {
                $status = false;
                $msg = 'Caught exception: '.  $e->getMessage();
        }
    }

    if($status == true)
    {
        $id_user = $payload->uid;
        $tipe = $payload->utipe;

        $enroll_cs = isset($_GET['session']) ? $_GET['session'] : ""; 
        $course = isset($_GET['course']) ? $_GET['course'] : ""; 
    
        try{

            $db2->where ("enroll_cs", $enroll_cs);
            $data = $db2->getOne ("enroll");

            if($data["enroll_status"] == 1)
            {   $status = false;
                $msg = "You already enrolled !";
                $hasil = $data["enroll_status"];
            }
            else
            {
                
                $status = true;
                //mulai insert
                $data = Array ();
                if($id_user!="")
                {
                    $data += array('enroll_userid' => $id_user);
                }
                if($course!="")
                {
                    $data += array('enroll_courseid' => $course);
                }
                if($enroll_cs!="")
                {
                    $data += array('enroll_cs' => $enroll_cs);
                }
               
                
                $hasil_eksekusi = false;

                
                    $data += array("enroll_id" => null);
                    $data += array('enroll_remark' => "");
                    $data += array('enroll_created_by' => null);
                    $data += array('enroll_created_at' => $tgl);
                    $data += array('enroll_modified_by' => null);
                    $data += array('enroll_modified_at' => $tgl);
                    if($db->insert ('enroll', $data))
                    {
                        $msg = "Insert Success";
                        $status = true;
                        $info = "success";
                    // echo json_encode( array("status" => true,"info" => "success","messages" => "Insert Success" ) );
                    }
                    else
                    {
                        $msg = "Insert Fail";
                        $status = false;
                        $info = $db->getLastError();
                    // echo json_encode( array("status" => false,"info" => $db->getLastError(),"messages" => "Insert Fail" ) );
                    }
            }
        }
        catch (Exception $e) 
        {
            $db->rollback();
            $status = false;
            $msg = 'Caught exception: '.  $e->getMessage();
        }
        echo json_encode( array("status" => $status,"info" => 'sukses', "messages" => $msg) );       

        
    }
    else
    {
        // $mode = isset($_GET['mode']) ? $_GET['mode'] : ""; 
        // $resi = isset($_GET['resi']) ? $_GET['resi'] : ""; 
        
        // if($mode == "lacak")
        // {
        //     try{

        //         $db->where ("no_resi", $resi);
        //         $transaksi = $db->getOne ("transaksi");

        //         if($transaksi["status_transaksi"])
        //         {   $status = true;
        //             $msg = "Get Data Success!";
        //             $hasil = $transaksi["status_transaksi"];
        //         }
        //         else
        //         {
        //             $status = false;
        //             $msg = "Get Data Error!";
        //         }
        //     }
        //     catch (Exception $e) 
        //     {
        //         $db->rollback();
        //         $status = false;
        //         $msg = 'Caught exception: '.  $e->getMessage();
        //     }
        // echo json_encode( array("status" => $status,"info" => 'sukses', "messages" => $msg,"hasil" => $hasil ,"resi" => $resi) );
        echo json_encode( array("status" => false,"info" => 'gagal', "messages" => "fail" ) );

        // }
    }
 

}

$db->disconnect();
?>