


<?php
$id_session = isset($_SESSION['i']) ? $_SESSION['i'] : "";

$maxfile = 1;
$filecount = 0;

$user_id = isset($_GET['user_id']) ? $_GET['user_id'] : ""; 

if($user_id!="" && $tipe_user=="ADMIN")
{
  $sql = "SELECT * FROM users WHERE user_id = '$user_id' "; 
  $result = $db->rawQuery($sql);//@mysql_query($sql);
}
else if ($user_id=="")
{
  $user_id = $id_session;
  $sql = "SELECT * FROM users WHERE user_id = '$user_id' "; 
  $result = $db->rawQuery($sql);//@mysql_query($sql);
}

?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile <?php // var_dump($result); ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User Profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="../uploads/user/<?=$result[0]['user_foto']?>"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center"><?=$result[0]['user_nama']?></h3>

                <p class="text-muted text-center"><?=$result[0]['user_tipe']?></p>

                <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                    <b>Username</b> <a class="float-right"><?=$result[0]['user_name']?></a>
                  </li>
                  <li class="list-group-item">
                    <b>Email</b> <a class="float-right"><?=$result[0]['user_email']?></a>
                  </li>
                  <li class="list-group-item">
                    <b>Phone</b> <a class="float-right"><?=$result[0]['user_hp']?></a>
                  </li>
                 
                  
                </ul>
              

                <!-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

         
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <!-- <li class="nav-item"><a class="nav-link active" href="#view" data-toggle="tab">View</a></li> -->
                  <!-- <li class="nav-item"><a class="nav-link active" href="#update" data-toggle="tab">Update</a></li> -->
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  

                  <div class="active tab-pane" id="update">
                    <form class="form-horizontal" id="userform" action="#"  enctype="multipart/form-data" method="post">

                      <div class="form-group row">
                        <label for="user_name" class="col-sm-2 col-form-label">Username</label>
                        <div class="col-sm-10">
                          <input type="text" disabled class="form-control" id="user_name" name="user_name" placeholder="username" value="<?=$result[0]['user_name']?>">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="user_pass" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" id="user_pass" name="user_pass" placeholder="password">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="user_nama" class="col-sm-2 col-form-label">Full Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="user_nama" name="user_nama" placeholder="Nama" value="<?=$result[0]['user_nama']?>">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="user_email" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                        <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email" value="<?=$result[0]['user_email']?>">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="user_nama" class="col-sm-2 col-form-label">Phone</label>
                        <div class="col-sm-10">
                          <input type="number" class="form-control" id="user_hp" name="user_hp" placeholder="Phone" value="<?=$result[0]['user_hp']?>">
                        </div>
                      </div>

                     
                      <!-- <option value='LEARNER'>LEARNER</option>
                            <option value='TRAINER'>TRAINER</option>
                            <option value='EMPLOYEE'>EMPLOYEE</option>
                            <option value='ADMIN'>ADMIN</option> -->
                      <div class="form-group row">
                        <label for="user_tipe" class="col-sm-2 col-form-label">Tipe</label>
                        <div class="col-sm-10">
                            <select class="form-control select2bs4" id="user_tipe" name="user_tipe">
                            <?php
                            if($tipe_user == "ADMIN") {
                                $arr_tipe_user = array("LEARNER" => "LEARNER","TRAINER" => "TRAINER","EMPLOYEE" => "EMPLOYEE","ADMIN" => "ADMIN");
                            }
                            else
                            {
                              $arr_tipe_user = array($tipe_user);
                            }

                                foreach ($arr_tipe_user as $key => $value)
                                {
                                  $selected = " ";
                                    if($result[0]['user_tipe'] == $value )
                                    {
                                      $selected = 'selected="selected"';
                                    }
                                    echo "<option value='".$key."' ".$selected ." >".$value."</option>" ;
                                }
                                ?>
                            
                            </select>
                        </div>
                      </div>

                      <?php
                          if($tipe_user == "ADMIN") {
                                $arr_tipe_user = array("0" => "nonaktif","1" => "aktif");
                      ?>
                      <div class="form-group row">
                        <label for="user_nama" class="col-sm-2 col-form-label">Status user</label>
                        <div class="col-sm-10">
                        <select class="form-control select2bs4" id="user_status" name="user_status">
                          <?php
                          foreach ($arr_tipe_user as $key => $value)
                          {
                            $selected = " ";
                              if($result[0]['user_status'] == $key )
                              {
                                $selected = 'selected="selected"';
                              }
                              echo "<option value='".$key."' ".$selected ." >".$value."</option>" ;
                          }
                          ?>
                          </select>
                        </div>
                      </div>
                        <?php } ?>  
                      

                      <div class="form-group row">
                     
                        <label for="user_foto" class="col-sm-2 col-form-label">Foto</label>
                        <div class="col-sm-10">
                        <?php if($result[0]['user_foto'] != null) {  $maxfile--;$filecount++; ?>
                                  <div id="preview1" class="col-lg-6 col-6" style="text-align:center;"> 
                                      <div class="small-box"  style="height: 100%;" onclick="">
                                      <div class="inner">
                                          <img src="<?php if(get_ext($result[0]['user_foto'])!="pdf"){ echo "../uploads/user/".$result[0]['user_foto']; }else{ echo "dist/img/pdf.png"; }?>" width="100" height="100" class=" " style="">

                                          <p><a href="<?php echo "../uploads/user/".$result[0]['user_foto']; ?>" target="_blank"  id="filename1"><?=$result[0]['user_foto']?></a></p>
                                      </div>
                                      <button onclick="hidden_div('<?=$result[0]['user_foto']?>',1);" type="button" class="btn btn-danger" >
                                      <i class="fas fa-times"> remove</i>
                                    </button>
                                      </div>
                                  </div>
                                <?php } ?>
                        <!-- <img src="dist/img/<?=$result[0]['user_foto']?>" alt="User Image"> -->
                        <!-- <input name="user_foto" type="file" class="form-control" placeholder="Input Foto:"/> -->
                        <div class="dropzone dropzone-previews" id="my-awesome-dropzone"></div>
                        </div>
                      </div>
                      
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit"  id="btnSubmit" name="btnSubmit" class="btn btn-primary">Submit</button>
                          <a href="home"><button type="button"  name="cancel" class="btn btn-secondary">Cancel</button></a>
                        </div>
                      </div>

                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  
<input type="hidden" id="maxfile" value="<?=$maxfile?>"/>
<input type="hidden" id="filecount" value="<?=$filecount?>"/>
<input type="hidden" id="filestatus1" value="1"/>
