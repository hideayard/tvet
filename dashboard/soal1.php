
    <h3>TEST</h3>
<div class="container">

    <div style="text-align:left;">
    <p class="MsoNormal"><span lang="EN-US">1. Apakah yang dimaksudkan dengan “Pendawai” mengikut tafsiran Peraturan-peraturan Elektrik 1994?</p>
    <!-- <br> -->
    <div class="form-group">
        <div class="radio">
            
            <label>
            <input type="radio" name="optionsRadios1" id="soal1A" value="A" checked="">
                <span class="text"> 
                A. Orang yang mahir dalam kerja-kerja pendawaian
                </span>
            </label>
            <!-- <br> -->

            <label>
            <input type="radio" name="optionsRadios1" id="soal1B" value="B" checked="">
                <span class="text"> 
                B. Orang yang akan menduduki peperiksaan pendawai
                </span>
            </label>
            <!-- <br> -->

            <label>
            <input type="radio" name="optionsRadios1" id="soal1C" value="C" checked="">
                <span class="text"> 
                C. Orang pengalaman yang bekerja dengan syarikat elektrik yang berdaftar
                </span>
            </label>
            <!-- <br> -->

            <label>
            <input type="radio" name="optionsRadios1" id="soal1D" value="D" checked="">
                <span class="text"> 
                D. Orang yang memegang Perakuan Kekompetenan sebagai Pendawai yang<!-- <br> -->
    dikeluarkan di bawah peraturan 50
                </span>
            </label>
        </div>
    </div>

<p class="MsoNormal"> 2. Mengikut Akta Bekalan Elektrik 1990, tidak seorang pun boleh memasang
    apa-apa <!-- <br> --> pendawaian baru atau pertambahan pendawaian yang ada pada mana-mana premis,<!-- <br> -->
    terlebih dahulu mendapat kelulusan bertulis daripada ...<!-- <br> --></p>
<!-- <br> --> 
<div class="form-group">
    <div class="radio">

        <label>
        <input type="radio" name="optionsRadios2" id="soal2A" value="A" checked="checked">
            <span class="text"> 
            A. Suruhanjaya Tenaga
            </span>
        </label>
        <!-- <br> -->

        <label>
        <input type="radio" name="optionsRadios2" id="soal2B" value="B" checked="">
            <span class="text"> 
            B. Jurutera penyelia yang berdaftar
            </span>
        </label>
        <!-- <br> -->

        <label>
        <input type="radio" name="optionsRadios2" id="soal2C" value="C" checked="">
            <span class="text"> 
            C. Kontraktor elektrik yang berdaftar
            </span>
        </label>
        <!-- <br> -->

        <label>
        <input type="radio" name="optionsRadios2" id="soal2D" value="D" checked="">
            <span class="text"> 
            D. Pemegang lesen atau pihak berkuasa bekalan
            </span>
        </label>
    </div>
</div>

    
    <p class="MsoNormal"> 3. Berapakah had umur bagi seseorang pemegang Perakuan Kekompetenan Pendawai<!-- <br> -->
    untuk membuat apa-apa kerja, tindakan atau perkara-perkara di bawah Peraturan-<!-- <br> -->
    Peraturan Elektrik 1994?</p>
<!-- <br> --> 
<div class="form-group">
    <div class="radio">
        <label>
        <input type="radio" name="optionsRadios3" id="soal3A" value="A" checked="checked">
            <span class="text"> 
            A. 60 tahun
            </span>
        </label>
        <!-- <br> -->

        <label>
        <input type="radio" name="optionsRadios3" id="soal3B" value="B" checked="">
            <span class="text"> 
            B. 65 tahun
            </span>
        </label>
        <!-- <br> -->

        <label>
        <input type="radio" name="optionsRadios3" id="soal3C" value="C" checked="">
            <span class="text"> 
            C. 70 tahun
            </span>
        </label>
        <!-- <br> -->

        <label>
        <input type="radio" name="optionsRadios3" id="soal3D" value="D" checked="">
            <span class="text"> 
            D. 75 tahun
            </span>
        </label>
    </div>
</div>
<br>
    <div class="form-group">
        <button class="btn btn-info btn-next">Submit Test</button>
    </div>
</div>
    
</div>