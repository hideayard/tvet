<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Digital TVET Academy (DATA)</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <style>
    /* Banner */

	#banner {
		/* background-color: #000000; */
		background-color: rgba(26, 25, 25, 0.97);
		background-image: url("dist/img/banner.jpg");
		background-size: cover;
		background-position: center center;
		background-repeat: no-repeat;
		color: #ffffff;
		padding: 6em 0;
		text-align: center;
		position: relative;
    height: 100%;
	}

		#banner:before {
			content: '';
			position: absolute;
			left: 0;
			top: 0;
			width: 100%;
			height: 100%;
			background: rgba(64, 72, 80, 0.55);
		}

		#banner .inner {
			color: #ffffff;

			position: relative;
			z-index: 1;
		}

			#banner .inner :last-child {
				margin-bottom: 0;
			}

		#banner h2, #banner h3, #banner h4, #banner h5, #banner h6 {
			color: #ffffff;
		}

		#banner .button.alt {
			box-shadow: inset 0 0 0 1px rgba(144, 144, 144, 0.75);
			color: #ffffff !important;
		}

			#banner .button.alt:hover {
				background-color: rgba(144, 144, 144, 0.275);
			}

			#banner .button.alt:active {
				background-color: rgba(144, 144, 144, 0.4);
			}

			#banner .button.alt.icon:before {
				color: #c1c1c1;
			}

		#banner .button {
			min-width: 12em;
		}

		#banner h2 {
			font-size: 3.5em;
			line-height: 1em;
			margin: 0 0 0.5em 0;
			padding: 0;
		}

		#banner p {
			font-size: 1.5em;
			margin-bottom: 2em;
		}

			#banner p a {
				color: #ffffff;
				text-decoration: none;
			}
    </style>
    <style>

label {
  display: -webkit-box;
  display: flex;
  cursor: pointer;
  font-weight: 500;
  position: relative;
  overflow: hidden;
  margin-bottom: 0.375em;

}
label input {
  position: absolute;
  left: -9999px;
}
label input:checked + span {
  background-color: #d6d6e5;
}
label input:checked + span:before {
  box-shadow: inset 0 0 0 0.4375em #00005c;
}
label span {
  display: -webkit-box;
  display: flex;
  -webkit-box-align: center;
          align-items: center;
  padding: 0.375em 0.75em 0.375em 0.375em;
  border-radius: 99em;
  -webkit-transition: 0.25s ease;
  transition: 0.25s ease;
}
label span:hover {
  background-color: #d6d6e5;
}
label span:before {
  display: -webkit-box;
  display: flex;
  flex-shrink: 0;
  content: "";
  background-color: #fff;
  width: 1.5em;
  height: 1.5em;
  border-radius: 50%;
  margin-right: 0.375em;
  -webkit-transition: 0.25s ease;
  transition: 0.25s ease;
  box-shadow: inset 0 0 0 0.125em #00005c;
}

.container {
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  display: -webkit-box;
  display: flex;
  -webkit-box-pack: center;
          justify-content: center;
  -webkit-box-align: center;
          align-items: center;
  padding: 20px;
}
    </style>
</head>
<body class="hold-transition layout-top-nav layout-navbar-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
      <a href="../" class="navbar-brand">
        <img src="dist/img/tvet_50.png" alt="TVET Logo" class="brand-image"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Digital TVET Academy (DATA)</span>
      </a>
      
     
 <!-- Right navbar links -->
    <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
            <li class="nav-item">
                <a class="btn btn-success" href="../" class="nav-link">1000 POINTS</a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">KHAIR NOORDIN (LEARNER)</a>
            </li>
           <!-- Messages Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-comments"></i>
                <span class="badge badge-danger navbar-badge">3</span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <a href="#" class="dropdown-item">
                    <!-- Message Start -->
                    <div class="media">
                    <img src="dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                    <div class="media-body">
                        <h3 class="dropdown-item-title">
                        Brad Diesel
                        <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                        </h3>
                        <p class="text-sm">Call me whenever you can...</p>
                        <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                    </div>
                    </div>
                    <!-- Message End -->
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <!-- Message Start -->
                    <div class="media">
                    <img src="dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                    <div class="media-body">
                        <h3 class="dropdown-item-title">
                        John Pierce
                        <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                        </h3>
                        <p class="text-sm">I got your message bro</p>
                        <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                    </div>
                    </div>
                    <!-- Message End -->
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <!-- Message Start -->
                    <div class="media">
                    <img src="dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                    <div class="media-body">
                        <h3 class="dropdown-item-title">
                        Nora Silvester
                        <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                        </h3>
                        <p class="text-sm">The subject goes here</p>
                        <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                    </div>
                    </div>
                    <!-- Message End -->
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a id="dropdownAccount" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><i class="far fa-user"></i></a>
                
                <ul aria-labelledby="dropdownAccount" class="dropdown-menu border-0 shadow">
                  <li> <a href="../" class="dropdown-item">Sign Out</a></li>
                </ul>
            </li>
            </ul>
            <!-- SEARCH FORM -->
            <form class="form-inline ml-0 ml-md-3">
            <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                </button>
                </div>
            </div>
            </form>
        </div>
      </ul>
    </div>
  </nav>
  <!-- /.navbar -->
<div class="row">
    <div class="col-lg-12">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Domestic Wiring</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Course Detail</li>
                    </ol>
                </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
            <div class="container-fluid">
           
                <!-- Main row -->
                <div class="row">
                <!-- Left col -->
                <div class="col-md-12">
                <div class="card">
              <div class="card-header bg-info">
                <h3 class="card-title">Domestic Wiring (<i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-secondary"></i>)</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                        <div class="col-9" style="text-align:center;">
                        <div id="step1">
                                <h3>1. Course Introduction</h3>
                                <?php //include("step1.php"); ?>

                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/xV0GxL8hiXk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <br>
                                    <br>
                                    <button class="btn btn-info btn-next">Mark This as Complete and go to next topic</button>
                            </div>

                            <div id="step2" style="display:none;">
                                <h3>2. Wiring drawing for single phase</h3>
                                <?php include("step2.php"); ?>
                                    <br>
                                    <br>
                                    <button class="btn btn-info btn-next">Mark This as Complete and go to next topic</button>
                            </div>

                            <div id="step3" style="display:none;">
                                <h3>3. Tutorial video on drawing</h3>
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/rJQ9_V9akpE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <br>
                                    <br>
                                    <button class="btn btn-info btn-next">Mark This as Complete and go to next topic</button>
                            </div>
                            
                            <div id="step4" style="display:none;">
                                <h3>4. Handling electrical equipment</h3>
                                <img class="img-fluid pad" src="dist/img/electrical-1.jpg" alt="Photo">

                                    <br>
                                    <br>
                                    <button class="btn btn-info btn-next">Mark This as Complete and go to next topic</button>
                            </div>
                            
                            <div id="step5" style="display:none;">
                                <h3>5. Tools in electrical wiring</h3>
                                <img class="img-fluid pad" src="dist/img/tools-electrical.jpg" alt="Photo">

                                    <br>
                                    <br>
                                    <button class="btn btn-info btn-next">Mark This as Complete and go to next topic</button>
                            </div>
                            
                            <div id="step6" style="display:none;">
                                <h3>6. hands on wiring</h3>
                                <img class="img-fluid pad" src="dist/img/hands-on-electrical.jpg" alt="Photo">
                                    <br>
                                    <br>
                                    <button class="btn btn-info btn-next">Mark This as Complete and go to next topic</button>
                            </div>
                            
                            <div id="step7" style="display:none;">

                                <?php include("soal1.php"); ?>

                            </div>

                            <div id="step8" style="display:none;">

                                <?php include("survey.php"); ?>

                            </div>


                            <br>
                            <br>

                            <div class="card">
              <div class="card-header d-flex p-0">
                <!-- <h3 class="card-title p-3">Tabs</h3> -->
                <ul class="nav nav-pills p-2">
                  <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab"><strong>Overview</strong></a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab"><strong>Q&A</strong></a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab"><strong>Announcement</strong></a></li>
                  
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                    A wonderful serenity has taken possession of my entire soul,
                    like these sweet mornings of spring which I enjoy with my whole heart.
                    I am alone, and feel the charm of existence in this spot,
                    which was created for the bliss of souls like mine. I am so happy,
                    my dear friend, so absorbed in the exquisite sense of mere tranquil existence,
                    that I neglect my talents. I should be incapable of drawing a single stroke
                    at the present moment; and yet I feel that I never was a greater artist than now.
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_2">
                    The European languages are members of the same family. Their separate existence is a myth.
                    For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ
                    in their grammar, their pronunciation and their most common words. Everyone realizes why a
                    new common language would be desirable: one could refuse to pay expensive translators. To
                    achieve this, it would be necessary to have uniform grammar, pronunciation and more common
                    words. If several languages coalesce, the grammar of the resulting language is more simple
                    and regular than that of the individual languages.
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_3">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    It has survived not only five centuries, but also the leap into electronic typesetting,
                    remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                    sheets containing Lorem Ipsum passages, and more recently with desktop publishing software
                    like Aldus PageMaker including versions of Lorem Ipsum.
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
                        </div>
                        <div class="col-3">
                            <div class="callout callout-info">
                                <h5><strong>Course Content</strong></h5>
                                
                                <p><strong>Section 1 : Single Phase Drawing</strong></p> 
                                <label>
                                <input  disabled  class="custom-control-input" type="checkbox" id="check1" value="check1">
                                    <span class="text"> 
                                    1. Course Intro
                                    </span>
                                </label>
                                <label>
                                <input  disabled  class="custom-control-input" type="checkbox" id="check2" value="check2">
                                    <span class="text"> 
                                    2. Wiring Drawing for Single Phase
                                    </span>
                                </label>
                                <label>
                                <input  disabled  class="custom-control-input" type="checkbox" id="check3" value="check3">
                                    <span class="text"> 
                                    3. Tutorial Video on Drawing
                                    </span>
                                </label>

                                   <br>
                                <!-- <p>Completion Section 1 :</p>  -->
                                <div class="progress-group">
                                    Completion Section 1 :
                                    <span class="float-right"><b><span id="complete1">0</span></b>/3</span>
                                    <div class="progress progress-sm">
                                        <div id="bar1" class="progress-bar bg-primary" style="width: 1%"></div>
                                    </div>
                                </div>
<hr>

                                <p><strong>Section 2 : Single Phase Wiring</strong></p> 
                                <label>
                                <input  disabled class="custom-control-input" type="checkbox" id="check4" value="check4">
                                    <span class="text"> 
                                    4. handling Electrical Equipment
                                    </span>
                                </label>
                                <label>
                                <input  disabled  class="custom-control-input" type="checkbox" id="check5" value="check5">
                                    <span class="text"> 
                                    5. Tools in Electrical Wiring
                                    </span>
                                </label>
                                <label>
                                <input  disabled  class="custom-control-input" type="checkbox" id="check6" value="check6">
                                    <span class="text"> 
                                    6. hands on Wiring
                                    </span>
                                </label>
                                
                                   <br>
                                <!-- <p>Completion Section 1 :</p>  -->
                                <div class="progress-group">
                                    Completion Section 2 :
                                    <span class="float-right"><b><span id="complete2">0</span></b>/3</span>
                                    <div class="progress progress-sm">
                                        <div id="bar2" class="progress-bar bg-primary" style="width: 1%"></div>
                                    </div>
                                </div>

                                <hr>

                                <label>
                                <input  disabled  class="custom-control-input" type="checkbox" id="check7" value="check7">
                                    <span class="text"> 
                                    TEST
                                    </span>
                                </label>
                               
                                <div style="text-align:center;">
                                <button onclick="showSurvey();" id="btnSurvey" disabled class="btn btn-info">Course Survey</button>
                                </div>

 
                            </div>
                            
                
                        </div>
                    </div>
                </div>
    
              </div>
              <input id="step" type="hidden" value="1" />
              <input id="complete1value" type="hidden" value="0" />
              <input id="complete2value" type="hidden" value="0" />
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                
            </div>
                   
                </div>
                <!-- /.col -->

                <div class="col-md-4">
                    

                   
                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div><!--/. container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
  </div>
</div>


  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Web Developer
    </div>
    <!-- Default to the left -->
    <strong>Developed &copy;2020 by <a href="https://microalvine.com">M4D Studio</a>.</strong>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>

<script>
    function showSurvey()
    {
        $( "#step8").show( "fast" );
    }
$( ".btn-submit" ).click(function() {
    Swal.fire({
                title: 'Are you sure to submit this survey?',
                text: "You won't be able to revert this!",
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Submit Now!'
                }).then((result) => {
                    console.log(result);
                if (result.isConfirmed || result.value) {
                    Swal.fire(
                    'Thank You!',
                    'Thank You for submitting the Survey',
                    'success'
                    );
                    setTimeout(function(){ window.location=''; }, 2000);

                }
                });
});

$( ".btn-next" ).click(function() {
    if(document.getElementById("step").value >0 && document.getElementById("step").value<8)
    {
        if(document.getElementById("step").value >= 1 && document.getElementById("step").value <=3)
        {
            document.getElementById("complete1value").value++;
            document.getElementById("complete1").innerHTML = String(document.getElementById("complete1value").value);
            document.getElementById("bar1").style.width = String(Math.floor((document.getElementById("complete1value").value / 3)* 100 )) + '%';

        }
        else if(document.getElementById("step").value >= 4 && document.getElementById("step").value <=6)
        {
            document.getElementById("complete2value").value++;
            document.getElementById("complete2").innerHTML = String(document.getElementById("complete2value").value);
            document.getElementById("bar2").style.width = String(Math.floor((document.getElementById("complete2value").value / 3)* 100 )) + '%';

        }
        else
        {
            document.getElementById("btnSurvey").disabled = false;
            Swal.fire(
                    'Congratulation!',
                    'You have successfully completed this Course.',
                    'success'
                    );
        }
        $( "#step"+document.getElementById("step").value ).hide( "fast" );
        $( "#check"+document.getElementById("step").value).prop('checked', true);
        console.log("btn next clicked",document.getElementById("step").value);
        document.getElementById("step").value++;
        // if(document.getElementById("step").value < 8)
        // {
        $( "#step"+document.getElementById("step").value ).show( "fast" );
        // }

    }    
    else{
        Swal.fire(
                    'Thank You!',
                    'Thank You for submitting the Survey',
                    'success'
                    );
    }
});

function enrollnow(id)
{
    switch(id)
    {
        case 1: {
                    Swal.fire({
                        title: 'Submit Course Voucher or Password',
                        input: 'text',
                        inputAttributes: {
                            autocapitalize: 'off'
                        },
                        showCancelButton: true,
                        confirmButtonText: 'Look up',
                        showLoaderOnConfirm: true,
                        preConfirm: (login) => {
                            return fetch(`//api.github.com/users/${login}`)
                            .then(response => {
                                if (!response.ok) {
                                throw new Error(response.statusText)
                                }
                                return response.json()
                            })
                            .catch(error => {
                                Swal.showValidationMessage(
                                `Request failed: ${error}`
                                )
                            })
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                        }).then((result) => {
                        if (result.isConfirmed) {
                            Swal.fire({
                            title: `${result.value.login}'s avatar`,
                            imageUrl: result.value.avatar_url
                            })
                        }
                        })
        }break;
        default : {
            Swal.fire({
                title: 'Are you sure to Enroll This Course?',
                text: "You won't be able to revert this!",
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Enroll Now!'
                }).then((result) => {
                    console.log(result);
                if (result.isConfirmed || result.value) {
                    Swal.fire(
                    'Enrolled!',
                    'You have Enrolled this Course.',
                    'success'
                    );

                }
                });
        }break;
    }
  
}
</script>
</body>
</html>
