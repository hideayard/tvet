


<?php
$id_session = isset($_SESSION['i']) ? $_SESSION['i'] : "";


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Courses</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Courses</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        
          <!-- /.col -->
          <div class="col-md-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <!-- <li class="nav-item"><a class="nav-link active" href="#view" data-toggle="tab">View</a></li> -->
                  <!-- <li class="nav-item"><a class="nav-link active" href="#update" data-toggle="tab">Update</a></li> -->
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  

                  <div class="active tab-pane" id="update">
                    <form class="form-horizontal" id="dataform" action="#"  enctype="multipart/form-data" method="post">

                      <div class="form-group row">
                        <label for="course_name" class="col-sm-2 col-form-label">Title</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="course_title" name="course_title" placeholder="Title">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="course_desc" class="col-sm-2 col-form-label">Description</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="course_desc" name="course_desc" placeholder="Description">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="course_nama" class="col-sm-2 col-form-label">Content</label>
                        <div class="col-sm-10">
                          <!-- <div class="mb-3"> -->
                                <textarea id="course_content" name="course_content"  class="textarea" placeholder="Content"
                                        style="width: 100%; height: 400px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                            <!-- </div> -->
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="course_type" class="col-sm-2 col-form-label">Type</label>
                        <div class="col-sm-10">
                        <input type="text" class="form-control" id="course_type" name="course_type" placeholder="Type">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="course_price" class="col-sm-2 col-form-label">PRICE</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="course_price" name="course_price" placeholder="Price">
                        </div>
                      </div>

                      <?php
                        //  if($tipe_user == "ADMIN") {
                                $arr_tipe_user = array("0" => "OFFLINE","1" => "ONLINE");
                      ?>
                      <div class="form-group row">
                        <label for="course_is_online" class="col-sm-2 col-form-label">IS ONLINE</label>
                        <div class="col-sm-10">
                        <select class="form-control select2bs4" id="course_is_online" name="course_is_online">
                          <?php
                          foreach ($arr_tipe_user as $key => $value)
                          {
                            $selected = " ";
                              echo "<option value='".$key."' ".$selected ." >".$value."</option>" ;
                          }
                          ?>
                          </select>
                        </div>
                      </div>
                        <?php //} ?>  

                        <?php
                        //  if($tipe_user == "ADMIN") {
                                $arr_tipe_user = array("0" => "NONAKTIF","1" => "AKTIF");
                      ?>
                      <div class="form-group row">
                        <label for="course_status" class="col-sm-2 col-form-label">STATUS</label>
                        <div class="col-sm-10">
                        <select class="form-control select2bs4" id="course_status" name="course_status">
                          <?php
                          foreach ($arr_tipe_user as $key => $value)
                          {
                            $selected = " ";
                             
                              echo "<option value='".$key."' ".$selected ." >".$value."</option>" ;
                          }
                          ?>
                          </select>
                        </div>
                      </div>
                        <?php //} ?>  
                      
                    

                      
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit"  id="btnSubmit" name="btnSubmit" class="btn btn-primary"><span class="fa fa-paper-plane"></span> Submit</button>
                          <a href="home"><button type="button"  name="cancel" class="btn btn-secondary">Cancel</button></a>
                        </div>
                      </div>

                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  
<input type="hidden" id="maxfile" value="<?=$maxfile?>"/>
<input type="hidden" id="filecount" value="<?=$filecount?>"/>
<input type="hidden" id="filestatus1" value="1"/>
