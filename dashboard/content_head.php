<div class="row">
                  <div class="col-12 col-sm-6 col-md-3">
                      <a href="course_catalog.php" >
                      <div class="info-box">
                      <span class="info-box-icon bg-info elevation-1"><i class="fas fa-book"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Courses</span>
                            <span class="info-box-number">
                            1,200
                            <!-- <small>%</small> -->
                            </span>
                        </div>
                      
                      <!-- /.info-box-content -->
                      </div>
                      <!-- /.info-box -->
                      </a>
                  </div>
                  <!-- /.col -->
                  <div class="col-12 col-sm-6 col-md-3">
                      <div class="info-box mb-3">
                      <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-certificate"></i></span>

                      <div class="info-box-content">
                          <span class="info-box-text">Certificates</span>
                          <span class="info-box-number">2,410</span>
                      </div>
                      <!-- /.info-box-content -->
                      </div>
                      <!-- /.info-box -->
                  </div>
                  <!-- /.col -->

                  <!-- fix for small devices only -->
                  <div class="clearfix hidden-md-up"></div>

                  <div class="col-12 col-sm-6 col-md-3">
                      <div class="info-box mb-3">
                      <span class="info-box-icon bg-success elevation-1"><i class="fas fa-tasks"></i></span>

                      <div class="info-box-content">
                          <span class="info-box-text">Complete task</span>
                          <span class="info-box-number">10,760</span>
                      </div>
                      <!-- /.info-box-content -->
                      </div>
                      <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                  <div class="col-12 col-sm-6 col-md-3">
                      <div class="info-box mb-3">
                      <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

                      <div class="info-box-content">
                          <span class="info-box-text">Members</span>
                          <span class="info-box-number">2,120</span>
                      </div>
                      <!-- /.info-box-content -->
                      </div>
                      <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->