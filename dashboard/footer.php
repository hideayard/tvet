<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Web Developer
    </div>
    <!-- Default to the left -->
    <strong>Developed &copy;2020 by <a href="https://microalvine.com">M4D Studio</a>.</strong>
  </footer>