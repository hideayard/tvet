
<div class="row">
    <div class="col-lg-12">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
            <div class="container-fluid">
                <!-- Info boxes -->
                

        <!-- Main row -->
        <div class="row">
                          <!-- Left col -->
                          <div class="col-md-8">
                            <div class="card">
                              <div class="card-header">
                                <h3 class="card-title">Hai <?=$_SESSION['nama']?>,</h3>
                              </div>
                              <!-- /.card-header -->
                              <div class="card-body">
                                    <p>Welcome to DIGITAL TVET ACADEMY,</p>
                                    <p>You are now enrols to take our free courses and all of our paid courses. Enioy your course!</p>
                                    <p>Click <a href="cs">here</a> to see more courses.</p>
                              </div>
                          </div>

                          <div class="card">
                                            <div class="card-header">
                                              <h3 class="card-title">Your Enrollment Courses</h3>
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                              <table class="table table-bordered">
                                                <thead>                  
                                                  <tr>
                                                    <th style="width: 10px">#</th>
                                                    <th>Category</th>
                                                    <th>Task</th>
                                                    <th>Progress</th>
                                                    <th>Date</th>
                                                    <th>Action</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $db->join("courses c", "c.course_id=e.enroll_courseid", "INNER");
                                                $db->join("course_session cs", "cs.cs_id=e.enroll_cs", "INNER");
                                                $db->where ("e.enroll_userid", $id);
                                                // $data = $db->getOne ("enroll");
                                                $enrolls = $db->get ("enroll e", null, "*");

                                                if ($db->count > 0)
                                                $i=0;
                                                foreach ($enrolls as $enroll) { 
                                                    // print_r ($enroll);
                                                    ?>

                                                   
                                                  <tr>
                                                    <td><?=$i+1?>.</td>
                                                    <td><?=$enroll["course_type"]?></td>
                                                    <td><?=$enroll["course_title"]?></td>
                                                    <td>
                                                      <div class="progress progress-xs">
                                                        <div class="progress-bar progress-bar-danger" style="width: 1%"></div>
                                                      </div>
                                                    </td>

                                                    <td><?=date("d", strtotime($enroll["cs_date_start"]))?> - <?=date("d M Y", strtotime($enroll["cs_date_end"]))?></td>
                                                    <td><button  class="btn btn-info"><span>Start Now!</span></button></td>
                                                  </tr>
                                                  <?php
                                                    $i++;
                                                }
                                                ?>
                                                  <!-- <tr>
                                                    <td>2.</td>
                                                    <td>Electronics</td>
                                                    <td><a href="course_catalog.php" >Transistor - Concept of Transistor </a></td>
                                                    <td>
                                                      <div class="progress progress-xs">
                                                        <div class="progress-bar bg-warning" style="width: 70%"></div>
                                                      </div>
                                                    </td>
                                                    <td><span class="badge bg-warning">70%</span></td>
                                                    <td>-</td>
                                                  </tr>
                                                  <tr>
                                                    <td>3.</td>
                                                    <td>Electronics</td>
                                                    <td>Transistor - NPN and PNP transistor</td>
                                                    <td>
                                                      <div class="progress progress-xs progress-striped active">
                                                        <div class="progress-bar bg-primary" style="width: 30%"></div>
                                                      </div>
                                                    </td>
                                                    <td><span class="badge bg-primary">30%</span></td>
                                                    <td>-</td>
                                                  </tr>
                                                  <tr>
                                                    <td>4.</td>
                                                    <td>Arduino</td>
                                                    <td>I/O Pins Arduino</td>
                                                    <td>
                                                      <div class="progress progress-xs progress-striped active">
                                                        <div class="progress-bar bg-success" style="width: 100%"></div>
                                                      </div>
                                                    </td>
                                                    <td><span class="badge bg-success">100%</span></td>
                                                    <td><button class="btn btn-info">Get Certificates</button></td>
                                                  </tr> -->
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- /.card-body -->
                                            <div class="card-footer clearfix">
                                              <ul class="pagination pagination-sm m-0 float-right">
                                                <li class="page-item"><a class="page-link" href="#">«</a></li>
                                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                <li class="page-item"><a class="page-link" href="#">»</a></li>
                                              </ul>
                                            </div>
                                        </div>
                            
                          </div>
                          <!-- /.col -->

                          <div class="col-md-4">
                          
                              <!-- Calendar -->
                                <div class="card bg-gradient-info">
                                  <div class="card-header border-0">

                                    <h3 class="card-title">
                                      <i class="far fa-calendar-alt"></i>
                                      Calendar
                                    </h3>
                                    <!-- tools card -->
                                    <div class="card-tools">
                                      <!-- button with a dropdown -->
                                      <div class="btn-group">
                                        <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" data-offset="-52">
                                          <i class="fas fa-bars"></i></button>
                                        <div class="dropdown-menu" role="menu">
                                          <a href="#" class="dropdown-item">Add new event</a>
                                          <a href="#" class="dropdown-item">Clear events</a>
                                          <div class="dropdown-divider"></div>
                                          <a href="#" class="dropdown-item">View calendar</a>
                                        </div>
                                      </div>
                                      <button type="button" class="btn btn-info btn-sm" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                      </button>
                                    
                                    </div>
                                    <!-- /. tools -->
                                  </div>
                                  <!-- /.card-header -->
                                  <div class="card-body pt-0">
                                    <!--The calendar -->
                                    <div id="calendar" style="width: 100%"></div>
                                  </div>
                                  <!-- /.card-body -->
                                </div>
                                <!-- /.card -->

                            
                              <!-- /.card-header -->
                              <div class="card-body p-0">
                                  <ul class="products-list product-list-in-card pl-2 pr-2">
                                  <li class="item">
                                      <div class="product-img">
                                      <img src="dist/img/default-150x150.png" alt="Product Image" class="img-size-50">
                                      </div>
                                      <div class="product-info">
                                      <a href="javascript:void(0)" class="product-title">Web Development - Advanced
                                          <span class="badge badge-warning float-right">$100</span></a>
                                      <span class="product-description">
                                          Asyncronous Data Request from frontend to Server.
                                      </span>
                                      </div>
                                  </li>
                                  <!-- /.item -->
                                  <li class="item">
                                      <div class="product-img">
                                      <img src="dist/img/default-150x150.png" alt="Product Image" class="img-size-50">
                                      </div>
                                      <div class="product-info">
                                      <a href="javascript:void(0)" class="product-title">Electronics - IoT System Fundamental
                                          <span class="badge badge-info float-right">$300</span></a>
                                      <span class="product-description">
                                          IoT Fundamental, Connecting Devices To Internet 
                                      </span>
                                      </div>
                                  </li>
                                  <!-- /.item -->
                                  <li class="item">
                                      <div class="product-img">
                                      <img src="dist/img/default-150x150.png" alt="Product Image" class="img-size-50">
                                      </div>
                                      <div class="product-info">
                                      <a href="javascript:void(0)" class="product-title">
                                          Web Development - Beginner <span class="badge badge-danger float-right">
                                          $150
                                      </span>
                                      </a>
                                      <span class="product-description">
                                          Web Programming Fundamental, HTML5, CSS.
                                      </span>
                                      </div>
                                  </li>
                                  <!-- /.item -->
                                  <li class="item">
                                      <div class="product-img">
                                      <img src="dist/img/default-150x150.png" alt="Product Image" class="img-size-50">
                                      </div>
                                      <div class="product-info">
                                      <a href="javascript:void(0)" class="product-title">DevOps - Beginner
                                          <span class="badge badge-success float-right">$399</span></a>
                                      <span class="product-description">
                                          Docker Containerization Fundamental
                                      </span>
                                      </div>
                                  </li>
                                  <!-- /.item -->
                                  </ul>
                              </div>
                              <!-- /.card-body -->
                              <div class="card-footer text-center">
                                  <a href="cs" class="uppercase">View All New Courses</a>
                              </div>
                              <!-- /.card-footer -->
                              </div>
                              <!-- /.card -->
                          </div>
                        <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        </div><!--/. container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
  </div>
</div>