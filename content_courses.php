<div class="site-section">
      <div class="container">


        <div class="row mb-5 justify-content-center text-center">
          <div class="col-lg-6 mb-5">
            <h2 class="section-title-underline mb-3">
              <span>Popular Courses</span>
            </h2>
            <p>There are so many great courses that you can enroll</p>
          </div>
        </div>

        <div class="row">
          <div class="col-12">
              <div class="owl-slide-3 owl-carousel">
                  <div class="course-1-item">
                    <figure class="thumnail">
                      <a href="detail_courses"><img src="images/course_1.jpg" alt="Image" class="img-fluid"></a>
                      <div class="price">RM1000</div>
                      <div class="category"><h3>Mobile Application</h3></div>  
                      
                      <!-- <div class=" center alert alert-primary">Online</div> -->
                      <!-- <div class="center alert alert-info" role="alert">Online2</div> -->
                    </figure>
                    <div class="course-1-content pb-4">
                      <h2>How To Create Mobile Apps Using Ionic - <span class="badge badge-pill badge-info">Online</span></h2>
                      
                      <div class="rating text-center mb-3">
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                      </div>
                      <p class="desc mb-4">Developing mobile application using IONIC to make beafitul interface.</p>
                      
                      <p><a href="detail_courses" class="btn btn-primary rounded-0 px-4">Enroll In This Course</a></p>
                    </div>
                  </div>
      
                  <div class="course-1-item">
                    <figure class="thumnail">
                      <a href="detail_courses"><img src="images/course_2.jpg" alt="Image" class="img-fluid"></a>
                      <div class="price">RM800</div>
                      <div class="category"><h3>Web Design</h3></div>  
                    </figure>
                    <div class="course-1-content pb-4">
                      <h2>How To Create Web Design using photoshop and css3  - <span class="badge badge-pill badge-info">Online</span></h2>
                      <div class="rating text-center mb-3">
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-secondary"></span>
                      </div>
                      <p class="desc mb-4">Developing web design using photoshop from scratch to the html template with css3</p>
                      <p><a href="detail_courses" class="btn btn-primary rounded-0 px-4">Enroll In This Course</a></p>
                    </div>
                  </div>
      
                  <div class="course-1-item">
                    <figure class="thumnail">
                      <a href="detail_courses"><img src="images/course_6.jpg" alt="Image" class="img-fluid"></a>
                      <div class="price">RM1000</div>
                      <div class="category"><h3>Programming C</h3></div>  
                    </figure>
                    <div class="course-1-content pb-4">
                      <h2>Fundamental programming for C language - <span class="badge badge-pill badge-info">Online</span></h2>
                      <div class="rating text-center mb-3">
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                      </div>
                      <p class="desc mb-4">Teaching student fundamental of programming C, from Logics, Algoritm, Primitive Data Type, Structure, Array, Looping, Function</p>
                      <p><a href="detail_courses" class="btn btn-primary rounded-0 px-4">Enroll In This Course</a></p>
                    </div>
                  </div>

                  <div class="course-1-item">
                    <figure class="thumnail">
                        <a href="detail_courses"><img src="images/flutter.jpg" alt="Image" class="img-fluid"></a>
                      <div class="price">RM1000</div>
                      <div class="category"><h3>Mobile Application</h3></div>  
                    </figure>
                    <div class="course-1-content pb-4">
                      <h2>How To Create Mobile Apps Using Flutter SDK - <span class="badge badge-pill badge-info">Online</span></h2>
                      <div class="rating text-center mb-3">
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                      </div>
                      <p class="desc mb-4">Flutter SDK is Google's UI toolkit for crafting beautiful, natively compiled applications for mobile, web, and desktop from a single codebase.</p>
                      <p><a href="detail_courses" class="btn btn-primary rounded-0 px-4">Enroll In This Course</a></p>
                    </div>
                  </div>
      
                  <div class="course-1-item">
                    <figure class="thumnail">
                        <a href="detail_courses"><img src="images/skm_course.jpg" alt="Image" class="img-fluid"></a>
                      <div class="price">RM1000</div>
                      <div class="category"><h3>Electrical Training Course</h3></div>  
                    </figure>
                    <div class="course-1-content pb-4">
                      <h2>Safety maintain and repair industrial electrical equipment - <span class="badge badge-pill badge-primary">Offline</span></h2>
                      <div class="rating text-center mb-3">
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                      </div>
                      <p class="desc mb-4">Our online electrical training courses prepare maintenance personnel to safely maintain and repair industrial electrical equipment.</p>
                      <p><a href="detail_courses" class="btn btn-primary rounded-0 px-4">Enroll In This Course</a></p>
                    </div>
                  </div>
      
                  <div class="course-1-item">
                    <figure class="thumnail">
                        <a href="detail_courses"><img src="images/arduino_course.jpg" alt="Image" class="img-fluid"></a>
                      <div class="price">RM1000</div>
                      <div class="category"><h3>Arduino Programming</h3></div>  
                    </figure>
                    <div class="course-1-content pb-4">
                      <h2>How To Create Robotic Programming with Arduino IDE - <span class="badge badge-pill badge-primary">Offline</span></h2>
                      <div class="rating text-center mb-3">
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                      </div>
                      <p class="desc mb-4">Arduino is an open-source electronics platform based on easy-to-use hardware and software. Arduino boards are able to read inputs - light on a sensor, a finger, etc.</p>
                      <p><a href="detail_courses" class="btn btn-primary rounded-0 px-4">Enroll In This Course</a></p>
                    </div>
                  </div>
      
              </div>
      
          </div>
        </div>

        
        
      </div>
    </div>