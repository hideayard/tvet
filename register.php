<div class="site-section">
    <div class="container">
    <div style="text-align:center;" id="error"><!-- error will be shown here ! --></div>
        <form class="form-register" id="register_form" >
<!-- <form action="user_detail" method="POST"> -->
        <div class="row justify-content-center">
            <div class="col-md-5">
                <div class="row">
                    <div class="col-md-12 form-group">
                        <label for="username">Username</label>
                        <input type="text" id="username" name="user_name" class="form-control form-control-lg">
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="email">Email</label>
                        <input type="email" id="email" name="user_email" class="form-control form-control-lg">
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="password">Password</label>
                        <input type="password" id="password" name="user_pass" class="form-control form-control-lg">
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="password2">Re-type Password</label>
                        <input type="password" id="password2" class="form-control form-control-lg">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                    <button name="btn-register" id="btn-register" class="btn btn-primary btn-block btn-flat"><i class="fa fa-user-plus"></i> &nbsp; Register Now</button>
                    </div>
                </div>
            </div>
        </div>
        
</form>
      
    </div>
</div>