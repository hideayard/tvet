<div class="section-bg style-1" style="background-image: url('images/about_1.jpg');">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <h2 class="section-title-underline style-2">
              <span>About Us</span>
            </h2>
          </div>
          <div class="col-lg-8">
            <p class="lead">Digital TVET Academy </p>
            <p>is a platform for education online that connecting teachers with learners to provide contemporaneous and uncontemporaneous courses which can help them in coming across new things. There are various websites which are offering you the most comprehensive educators which can teach you by giving you a platform to choose your own way of learning and sometimes they also provide you the live instructions. Not only learning is done but you can check your performance by giving regular tests or by making assignments related to the topic which you have studied and if it is not possible for one to study with the live instructions then they can use self-paced instructions including hosting presentations, videos, screen sharing, break out of rooms, interactive whiteboards and much more.</p>
            <p><a href="#">Read more</a></p>
          </div>
        </div>
      </div>
    </div>