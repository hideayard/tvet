<div class="site-section">
    <div class="container">


        <div class="row justify-content-center">
            <div class="col-md-5">
                <div class="row">
                    <div class="col-md-12 form-group">
                        <label for="firstname">First name</label>
                        <input type="text" id="firstname" class="form-control form-control-lg">
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="surname">Surname</label>
                        <input type="text" id="surname" class="form-control form-control-lg">
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="gender">Gender</label>
                        <input type="text" id="gender" class="form-control form-control-lg">
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="city">City/Town</label>
                        <input type="text" id="city" class="form-control form-control-lg">
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="country">Country</label>
                        <input type="text" id="country" class="form-control form-control-lg">
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="residence">Residence</label>
                        <input type="text" id="residence" class="form-control form-control-lg">
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="nationality">Nationality</label>
                        <input type="text" id="nationality" class="form-control form-control-lg">
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="interest">Interest</label>
                        <input type="text" id="interest" class="form-control form-control-lg">
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="organization">Organization</label>
                        <input type="text" id="organization" class="form-control form-control-lg">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <input type="submit" value="Save" class="btn btn-primary btn-lg px-5">
                    </div>
                </div>
            </div>
        </div>
        

      
    </div>
</div>