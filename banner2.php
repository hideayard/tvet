<div class="site-section ftco-subscribe-1 site-blocks-cover pb-4" style="background-image: url('images/bg_1.jpg')">
    <div class="container">
      <div class="row align-items-end justify-content-center text-center">
        <div class="col-lg-7">
          <h2 class="mb-0"><?=ucwords(str_replace("content","",str_replace("_"," ",$page)))?></h2>
          <p>This is module <?=str_replace("content","",str_replace("_"," ",$page))?>.</p>
        </div>
      </div>
    </div>
  </div> 