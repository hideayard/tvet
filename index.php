<?php
date_default_timezone_set('Asia/Singapore');
session_start();
// error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once("dashboard/tokenlogin.php");

$secret = "B15m1ll4#";

$page = isset($_GET['page']) ? $_GET['page'] : "home"; 

$payload = null;

require_once ('dashboard/config/MysqliDb.php');
include_once ("dashboard/config/db.php");
include_once ("dashboard/config/functions.php");

$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <title>Digital TVET Academy (DATA) - 2020</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


  <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
  <link rel="stylesheet" href="fonts/icomoon/style.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="dashboard/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/jquery-ui.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">

  <link rel="stylesheet" href="css/jquery.fancybox.min.css">

  <link rel="stylesheet" href="css/bootstrap-datepicker.css">

  <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

  <link rel="stylesheet" href="css/aos.css">
  <link href="css/jquery.mb.YTPlayer.min.css" media="all" rel="stylesheet" type="text/css">

  <link rel="stylesheet" href="css/style.css">

<style>
  .center {
  margin: auto;
  text-align: center;
  vertical-align: middle; 
}
  </style>

<?php
if (file_exists("c_".$page.".php")) 
{
  include_once ("c_".$page.".php");
}
?>

</head>

<body onload="checkCookie();" data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

  <div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>


    <div class="py-2 bg-light">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-9 d-none d-lg-block">
            <a href="#" class="small mr-3"><span class="icon-question-circle-o mr-2"></span> Have a questions?</a> 
            <a href="#" class="small mr-3"><span class="icon-phone2 mr-2"></span> +60 13-702 4102</a> 
            <a href="#" class="small mr-3"><span class="icon-envelope-o mr-2"></span> dashboard@digitaltvet-academy.com</a> 
          </div>
          <div class="col-lg-3 text-right">
            <?php if(isset($_SESSION['i'])) { ?>
              <a href="dashboard" class="small mr-3 "><span class="fa fa-user"></span> <?=$_SESSION['nama']?></a> 

          
            <?php
            }else{
            ?>
              <a href="login" class="small mr-3"><span class="icon-unlock-alt"></span> Log In</a>
            <a href="register" class="small btn btn-primary px-4 py-2 rounded-0"><span class="icon-users"></span> Register</a>
                   
            <?php
            }
            ?>
          </div>
        </div>
      </div>
    </div>
    <header class="site-navbar py-4 js-sticky-header site-navbar-target" role="banner">

      <div class="container">
        <div class="d-flex align-items-center">
          <div class="site-logo">
            <a href="home" class="d-block">
              <img src="images/tvet_50.png" alt="Image" class="img-fluid">        
              <span class="brand-text font-weight-light center">Digital TVET Academy</span>
            </a>
          </div>
          <div class="mr-auto">
            <nav class="site-navigation position-relative text-right" role="navigation">
              <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
                <li class="active">
                  <a href="home" class="nav-link text-left">Home</a>
                </li>
               
                <li class="has-children">
                  <a href="courses" class="nav-link text-left">Courses</a>
                  <ul class="dropdown">
                    <li><a href="free_courses">Free Courses</a></li>
                    <li><a href="courses">Paid Courses</a></li>
                  </ul>
                </li>
                <li>
                  <a href="services" class="nav-link text-left">Services</a>
                </li>
                <li>
                  <a href="#" class="nav-link text-left">Topic</a>
                </li>
                <li>
                  <a href="#" class="nav-link text-left">Stories</a>
                </li>

                <li>
                    <a href="contact" class="nav-link text-left">Contact</a>
                  </li>
        <?php if( isset($_SESSION['nama']) )  { ?>
                <li class="has-children">
                  <a href="courses" class="nav-link text-left">User</a>
                  <ul class="dropdown">
                  <li><a href="dashboard/">Dashboard</a></li>
                  <li><a href="profile">Profile</a></li>
                    <li><a href="logout">Sign Out</a></li>
                  </ul>
                </li>

                 
      <?php } ?>
              </ul>                                                                                                                                                                                                                                                                                          </ul>
            </nav>

          </div>
          <!-- <div class="ml-auto">
            <div class="social-wrap">
              <a href="#"><span class="icon-facebook"></span></a>
              <a href="#"><span class="icon-twitter"></span></a>
              <a href="#"><span class="icon-linkedin"></span></a>

              <a href="#" class="d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-black"><span
                class="icon-menu h3"></span></a>
            </div>
          </div> -->
         
        </div>
      </div>

    </header>

    
    <?php 
    
    if($page == "" || $page == "home")
    {
      include_once ("content_video.php");

      include_once ("banner.php");

      include_once ("content_why.php");
      include_once ("content_courses.php");
      include_once ("content_about.php");
      // include_once ("content_testi.php");
      // include_once ("content_philosophy.php");
      include_once ("content_news.php");
      include_once ("subscribe.php");
    }
    else
    {
      include_once ("banner2.php");

        if (file_exists("".$page.".php")) 
        {
          include_once ("".$page.".php");
        }
        else 
        {
          include_once ("error.php");
        }
    }
     
     ?>
    <?php //var_dump($payload); ?>
    <?php include_once ("footer.php"); ?>
    
    <input type="hidden" id="token" name="token" value="<?=( isset($_SESSION['token'])? $_SESSION['token'] : "" )?>" />

  </div>
  <!-- .site-wrap -->


  <!-- loader -->
  <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#51be78"/></svg></div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/validation.min.js"></script>

  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/jquery.countdown.min.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.fancybox.min.js"></script>
  <script src="js/jquery.sticky.js"></script>
  <script src="js/jquery.mb.YTPlayer.min.js"></script>




  <script src="js/main.js"></script>
  <?php
if (file_exists("s_".$page.".php")) 
{
  include_once ("s_".$page.".php");
}
?>

  <script>
    function isEmpty(obj) {
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                return false;
        }
          return true;
      }

      function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
            c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
            }
        }
        return "";
        }

        function checkCookie() {
          var token = getCookie("token");
          var user = getCookie("user");
          if (user != "") {
              console.log("Welcome again " ,user);
          } else {
              // user = prompt("Please enter your name:", "");
              // if (user != "" && user != null) {
              // setCookie("username", user, 365);
              // }
          }
        }

        
    </script>
</body>

</html>