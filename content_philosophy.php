<div class="section-bg style-1" style="background-image: url('images/hero_1.jpg');">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
            <span class="icon flaticon-mortarboard"></span>
            <h3>Our Philosphy</h3>
            <p>A collection of basic notions concerning the world and our way of considering it. Divided into two investigations: one concerned with the theory of knowledge, and the other, with the philosophical notion of the world.</p>
          </div>
          <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
            <span class="icon flaticon-school-material"></span>
            <h3>Academics Principle</h3>
            <p>Academic integrity is the commitment to behave with honesty, fairness, trust, respect and responsibility in all your academic work.</p>
          </div>
          <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
            <span class="icon flaticon-library"></span>
            <h3>Key of Success</h3>
            <p> Determination, Skill, Passion, Discipline And Luck. Determination is both a firmness of purpose and a willingness to work unceasingly towards that purpose in spite of any obstacles.</p>
          </div>
        </div>
      </div>
    </div>