<?php
$table = "banner";
$txt_field= "b_title,b_desc,b_link,b_foto,b_status";
$txt_label = "Title,Description,Link,b_foto,Status";
$q_field = explode(",",$txt_field);
$q_label = explode(",",$txt_label);
$i=1;$query = "select ".$q_field[0] ;//." as " .$q_label[0];
for($i;$i<count($q_field);$i++)
{
    $query .= ",".$q_field[$i];// ." as " .$q_label[$i];
}
$query .= " from $table";
// echo $query;
$data_query = $db->rawQuery($query);
?>

<div class="hero-slide owl-carousel site-blocks-cover">
<?php
 foreach ($data_query as $key => $value)
 {

?>
    <div class="intro-section" style="background-image: url('<?=$value["b_foto"]?>');">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-12 mx-auto text-center" data-aos="fade-up">
            <h1><a class="text-white" href="<?=$value["b_link"]?>"><?=$value["b_title"]?></a></h1>
            </div>
          </div>
        </div>
      </div>
<?php
 }
?>
      <!-- <div class="intro-section" style="background-image: url('images/electrical-1.jpg');">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-12 mx-auto text-center" data-aos="fade-up">
              <h1>You Can Learn Anything</h1>
            </div>
          </div>
        </div>
      </div> -->

      

</div>
    