<script>
        $(function(){
            var form = $(".register-form");

            form.css({
                opacity: 1,
                "-webkit-transform": "scale(1)",
                "transform": "scale(1)",
                "-webkit-transition": ".5s",
                "transition": ".5s"
            });

            /* validation */
     $("#register_form").validate({
      rules:
      {
            password: {
            required: true,
            },
            email: {
            required: true,
            email: true
            },
            username: {
            required: true,
            //email: true
            },
       },
       messages:
       {
            password:{
                      required: "please enter your password"
                     },
            email: "please enter your email address",
            username: "please enter your username address",
       },
       submitHandler: submitForm    
       });  
       /* validation */
       
       /* register submit */
       function submitForm()
       {        
            var data = $("#register_form").serialize();
                
            $.ajax({
                
            type : 'POST',
            url  : 'dashboard/actionregister.php',
            data : data,
            beforeSend: function()
            {   
                $("#error").fadeOut();
                $("#btn-register").html('<i class="fa fa-sync fa-spin"></i> &nbsp; Registering..');
            },
            success :  function(response)
               {          
                 try{
                        rv = JSON.parse(response);
                        if(isEmpty(rv) || rv.status==false)
                        {
                            console.log("NO DATA : ", response);
                            $("#error").fadeIn(500, function(){       
                                if(rv.messages)        
                                {
                                    $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span>'+rv.messages+' </div>');
                                }       
                                else
                                {
                                    $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span>Username Already Exist.! </div>');
                                }  
                            $("#btn-register").html('<span class="fa fa-user-plus"></span> &nbsp; Register Now');
                                    });
                        }
                        else
                        {
                          if(rv.status==true)
                          {
                            $("#error").fadeIn(500, function(){   
                                $("#error").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Register Now Success !</div>');
                                $("#btn-register").html('<span class="fa fa-user-plus"></span> &nbsp; Register Now');
                            });
                            console.log("SUCCESS : ", rv);
                            // setCookie("token", rv.info, 300);
                            // setCookie("user", rv.user, 300);
                            setTimeout(function(){ window.location="index.php"; }, 1000);
                            // window.location="index.php";
                            
                          }

                        }
                 }   
                 catch (e) {
                   
                  $("#error").fadeIn(500, function(){                        
                      $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Error :  '+e+' !</div>');
                      $("#btn-register").html('<span class="fa fa-user-plus"></span> &nbsp; Register Now');
                              });
                  
                console.log("ERROR : ", e);

                 }           
                    
              }
            });
                return false;
        }
       /* register submit */
        });

        
    </script>