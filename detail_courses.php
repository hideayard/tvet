<div class="custom-breadcrumns border-bottom">
    <div class="container">
      <a href="index.php">Home</a>
      <span class="mx-3 icon-keyboard_arrow_right"></span>
      <a href="courses.html">Courses</a>
      <span class="mx-3 icon-keyboard_arrow_right"></span>
      <span class="current">Courses</span>
    </div>
  </div>

  <div class="site-section">
      <div class="container">
          <div class="row shadow p-3 mb-5 bg-white rounded">
              <div class="col-md-6 mb-4">
                  <p>
                      <img src="images/course_6.jpg" alt="Image" class="img-fluid">
                  </p>
              </div>
              <div class="col-lg-5 ml-auto align-self-center">
                      <h2 class="section-title-underline mb-5">
                          <span>Fundamental programming for C language</span>
                      </h2>
                      
                      <p><strong class="text-black d-block">Teacher:</strong> Craig Daniel</p>
                      <p class="mb-5"><strong class="text-black d-block">Hours:</strong> 8:00 am &mdash; 9:30am</p>
                      
  
                  </div>
          </div>
          <div class="row shadow p-3 mb-5 bg-white rounded">
            <div class="col-md-3">
                <table>
                    <tr><td class="align-middle" rowspan="2"><button type="button" class="btn btn-lg btn-primary" disabled><i class="fas fa-play-circle"></i></button></td><td style="padding-left:5px;"> Online</td></tr>
                    <tr><td style="padding-left:5px;"> 3-7 January 2021</td></tr>
                    <tr><td colspan="2"><hr></td></tr>
                    <tr><td></td><td style="padding-left:5px;"> Next course</td></tr>
                    <tr><td></td><td style="padding-left:5px;"> 10-14 June 2021</td></tr>
                </table>
               
            </div>

            <div class="col-md-3">
                <table>
                    <tr><td class="align-middle" rowspan="2"><button type="button" class="btn btn-lg btn-primary" disabled>
                    <i class="fas fa-calendar-check"></i></button></td><td style="padding-left:5px;"> Application dateline</td></tr>
                    <tr><td style="padding-left:5px;"> 25 December 2020</td></tr>
                    <tr><td colspan="2"><hr></td></tr>
                    <tr><td class="align-middle" rowspan="2"><button type="button" class="btn btn-lg btn-primary" disabled>
                    <i class="fas fa-dollar-sign"></i></button></td><td style="padding-left:5px;"> Price</td></tr>
                    <tr><td style="padding-left:5px;"> RM1000</td></tr>
                </table>
               
            </div>

            <div class="col-md-3">
                <table>
                    <tr><td class="align-middle" rowspan="2"><button type="button" class="btn btn-lg btn-primary" disabled>
                    <i class="fas fa-info-circle"></i></button></td><td style="padding-left:5px;"> Email :</td></tr>
                    <tr><td style="padding-left:5px;"> enquiry@data.com </td></tr>
                    <tr><td colspan="2"><hr></td></tr>
                    <tr><td class="align-middle" rowspan="2"><button type="button" class="btn btn-lg btn-primary" disabled>
                    <i class="fas fa-qrcode"></i></button></td><td style="padding-left:5px;"> Code</td></tr>
                    <tr><td style="padding-left:5px;"> SKE1019</td></tr>
                </table>
               
            </div>

            <div class="col-md-3 center">
                <table class="center">
                <tr><td class=""><p> <button type="button" class="border border-info"> Download Information Note </button> </p></td></tr>
                <tr><td> <p><button type="button" class="btn btn-md btn-primary">Paid Now</button></p></td></tr>

                </table>
               
            </div>

          </div>

          <div class="row shadow p-3 mb-5 bg-white rounded">
            <div class="col-md-12 ">
            <h3>Introduction to the course</h3>
                <p>Teaching student fundamental of programming C :</p>
    
                <ul class="ul-check primary list-unstyled mb-5">
                    <li>Logics & Algoritm</li>
                    <li>Primitive Data Type</li>
                    <li>Structure</li>
                    <li>Array </li>
                    <li>Looping </li>
                    <li>Function </li>
                </ul>

                <p>
                    <a href="dashboard/course_detail.php" class="btn btn-primary rounded-0 btn-lg px-5">Enroll</a>
                </p>
            </div>
          </div>
      </div>
  </div>