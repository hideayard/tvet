<div class="site-section ftco-subscribe-1" style="background-image: url('images/bg_1.jpg')">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-7">
            <h2>Subscribe to us!</h2>
            <p>You can have the best deal from us first when we have promo,</p>
            <div style="text-align:center;" id="error"><!-- error will be shown here ! --></div>
          </div>
          <div class="col-lg-5">
            <form class="d-flex" id="subscribe_form" >
              <input type="text" id="subs_email" name="subs_email" class="rounded form-control mr-2 py-3" placeholder="Enter your email">
              <button name="btn-subs" id="btn-subs" class="btn btn-primary rounded py-3 px-4" type="submit">Subscribe</button>
            </form>
          </div>
        </div>
      </div>
    </div> 
