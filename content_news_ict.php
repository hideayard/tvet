
<div class="custom-breadcrumns border-bottom">
    <div class="container">
      <a href="index.php">Home</a>
      <span class="mx-3 icon-keyboard_arrow_right"></span>
      <a href="courses.html">Courses</a>
      <span class="mx-3 icon-keyboard_arrow_right"></span>
      <span class="current">Courses</span>
    </div>
  </div>

  <div class="site-section">
      <div class="container">
          <div class="row shadow p-3 mb-5 bg-white rounded">
              <div class="col-md-6 mb-4">
                  <p>
                      <img src="images/course_6.jpg" alt="Image" class="img-fluid">
                  </p>
              </div>
              <div class="col-lg-6 ml-auto align-self-center">
                      <h2 class="section-title-underline mb-5">
                          <span>ICT AND DIGITAL LITERACY FOR TEACHERS AND INSTRUCTORS</span>
                      </h2>
                      
                      <p style="text-align:center;"><img src="images/edu_ori.jpg" alt="Image" class="img-fluid"></p>
                      
  
                  </div>
          </div>
         
          <!-- <div class="row shadow p-3 mb-5 bg-white rounded">
            <div class="col-md-3">
                <table>
                    <tr><td class="align-middle" rowspan="2"><button type="button" class="btn btn-lg btn-primary" disabled><i class="fas fa-play-circle"></i></button></td><td style="padding-left:5px;"> Online</td></tr>
                    <tr><td style="padding-left:5px;"> 3-7 January 2021</td></tr>
                    <tr><td colspan="2"><hr></td></tr>
                    <tr><td></td><td style="padding-left:5px;"> Next course</td></tr>
                    <tr><td></td><td style="padding-left:5px;"> 10-14 June 2021</td></tr>
                </table>
               
            </div>

            <div class="col-md-3">
                <table>
                    <tr><td class="align-middle" rowspan="2"><button type="button" class="btn btn-lg btn-primary" disabled>
                    <i class="fas fa-calendar-check"></i></button></td><td style="padding-left:5px;"> Application dateline</td></tr>
                    <tr><td style="padding-left:5px;"> 25 December 2020</td></tr>
                    <tr><td colspan="2"><hr></td></tr>
                    <tr><td class="align-middle" rowspan="2"><button type="button" class="btn btn-lg btn-primary" disabled>
                    <i class="fas fa-dollar-sign"></i></button></td><td style="padding-left:5px;"> Price</td></tr>
                    <tr><td style="padding-left:5px;"> RM1000</td></tr>
                </table>
               
            </div>

            <div class="col-md-3">
                <table>
                    <tr><td class="align-middle" rowspan="2"><button type="button" class="btn btn-lg btn-primary" disabled>
                    <i class="fas fa-info-circle"></i></button></td><td style="padding-left:5px;"> Email :</td></tr>
                    <tr><td style="padding-left:5px;"> enquiry@data.com </td></tr>
                    <tr><td colspan="2"><hr></td></tr>
                    <tr><td class="align-middle" rowspan="2"><button type="button" class="btn btn-lg btn-primary" disabled>
                    <i class="fas fa-qrcode"></i></button></td><td style="padding-left:5px;"> Code</td></tr>
                    <tr><td style="padding-left:5px;"> SKE1019</td></tr>
                </table>
               
            </div>

            <div class="col-md-3 center">
                <table class="center">
                <tr><td class=""><p> <button type="button" class="border border-info"> Download Information Note </button> </p></td></tr>
                <tr><td> <p><button type="button" class="btn btn-md btn-primary">Paid Now</button></p></td></tr>

                </table>
               
            </div>

          </div> -->

          <div class="row shadow p-3 mb-5 bg-white rounded">
            <div class="col-md-12 ">
            <h3 style="text-align:center;">Introduction to the course</h3>
                <p>Transformation to online and distance learning due to Covid-19 is inevitable as this is the only way for the teachers and instructors to create a connection with their students. Challenges faced by most of the TVET institutions to deliver quality education are multifold. One of it is the skill and competency of the teachers and instructors in designing and developing their digital content. While students are coming from different geographical area with distinctive learning style, teachers and instructors must be creative and skillful in creating engaging and interactive content. Hence, students will enjoy an exciting learning experience and thus enhance their learning curve without neglecting the achievement of the learning outcome.    </p>
                <br>
                <h3 style="text-align:center;">Training Objectives</h3>
                <p>This training will focus on the skills development of teachers and instructors in designing and developing their digital content. At the end of the training, the participants will be able to:</p>
                <p>i- Understand the concept of online and distance learning</p>
                <p>ii- Design and develop digital content for student’s learning</p>
                <p>iii- Publish digital content according to the environment’s setting</p>
                <br>
                <h3 style="text-align:center;">Participants</h3>
                <p>The target participants for this training are teachers, instructors, trainers and lecturers. Curriculum content designers, administrators and managers of institutions are also encouraged to participate.</p>
                <br>
                <h3 style="text-align:center;">Delivery Method</h3>
                <p>This training will be conducted in 2 days. All modules will be delivered online through this platform:</p>
                <p style="text-align:center;"><a href="www.digitaltvet-academy.com" >www.digitaltvet-academy.com</a></p>
                <p>A practical session will be conducted physically subject to permission by the National Security Council on the second day. Participants are required to develop a complete set of teaching materials in the given time to obtain training completion certificate.</p>
                <!-- button -->
                <!-- <p>
                    <a href="dashboard/course_detail.php" class="btn btn-primary rounded-0 btn-lg px-5">Enroll</a>
                </p> -->
            </div>
          </div>

          <div class="row shadow p-3 mb-5 bg-white rounded">
            <div class="col-md-12 ">
            <h3 style="text-align:center;">There are 4 topics that will be covered in this training:</h3>
            <hr>
            <p><strong>Topic 1: Digital Skill and Learning Platform</strong></p>
            <ul>
            <li>Digital Skill</li>
            <li>Open Educational Resources (OER)</li>
            <li>Introduction to learning platform
            <ul>
            <li>Purpose</li>
            <li>Benefits/Advantages</li>
            <li>Key Features</li>
            <li>How can the students benefit from this platform?</li>
            <li>Task for Educators/Teachers (What can the teachers do?)</li>
            </ul>
            </li>
            </ul>
            <p><strong>&nbsp;</strong></p>
            <p><strong>Topic 2: Augmented Reality (AR)</strong></p>
            <ul>
            <li>Fundamentals of AR</li>
            <li>Platforms of AR</li>
            <li>Applications of AR</li>
            <li>Practical session of AR</li>
            </ul>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p><strong>Topic 3: Virtual Reality (VR) and 360 degree </strong></p>
            <p>- Introduction to VR</p>
            <p>&nbsp;- Experiencing VR</p>
            <p>&nbsp;- Development environment and toolset</p>
            <p>&nbsp;- Setting up the Development environment</p>
            <p>&nbsp;- Developing VR and 360-degree content</p>
            <p><strong>&nbsp;</strong></p>
            <p><strong>Topic 4: Developing Engaging and Interactive Content</strong></p>
            <ul>
            <li>Introduction</li>
            <li>Learning theories</li>
            <li>How to design your content</li>
            <li>How to develop content</li>
            <li>Creating Audio, Video, Power point and Animation</li>
            </ul>
            <p>&nbsp;</p>
            <p><strong>Project </strong></p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Design your own digital content and publish it online</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p><strong>Registration</strong></p>
            <p>&nbsp;</p>
            <p>Space for this training is limited to 25 participants per session, and participants will be registered on a first come, first serve basis.</p>
            <p>There are 3 sessions all together:</p>
            <p>&nbsp;</p>
            <p>Session 1 is for internal, which is staff from UTHM.</p>
            <p>Session 2 and 3 is opened for participants outside form UTHM.</p>
            <p>&nbsp;</p>
            <p>This training will run with identical content. The proposed that is as follow:</p>
            <p>&nbsp;</p>
            <!-- <p>Pick a date :</p> -->
            <div class="col-md-6 form-group">
                  <label for="tdate">Pick a Date :</label>
                  <select name="tdate" id="tdate" class="form-control form-control-lg">
                  <option value="1">Session 1 : 11-12 January 2021</option>
                  <option value="2">Session 2 : 18-19 January 2021</option>
                  <option value="3">Session 3 : 25-26 January 2021</option>
                  </select>
              </div>

            <p>Please reserve your seat by filling up this form</p>
            <p> 
            <button onclick="actionEnroll();" class="btn btn-primary rounded-0 btn-lg px-5">Enroll Now</button>
            <!-- <a href="#" onclick="actionenroll()" class="btn btn-primary rounded-0 btn-lg px-5">Enroll Now</a> -->
            </p>
            <p>&nbsp;</p>
            <p><strong>Contact Information</strong></p>
            <p>&nbsp;</p>
            <p>For more information, please contact:</p>
            <p>Ts. Dr. Affero Ismail</p>
            <p><a href="mailto:affero@uthm.edu.my">affero@uthm.edu.my</a></p>
            <p>+6019-7789843</p>
                <!-- button -->
              
            </div>
          </div>


<div class="row shadow p-3 mb-5 bg-white rounded">
            <div class="col-md-12 center">
            <h3>Training Schedule</h3>
            <p>Two-day training will be conducted in synchronous mode using online platform. Participants are required to complete all self-paced learning modules in the platform (www.digitaltvet-academy.com) before attending this two-day training.  </p>
            <table border="1">
<tbody>
<tr class="bg-primary text-white">
<td style="width: 7%;">
<p><strong>Day / Time</strong></p>
</td>
<td style="width: 13%;">
<p><strong>8.30 -&nbsp;</strong><strong>10.30 (am)</strong></p>
</td>
<td style="width: 13%;">
<p><strong>10.30 -&nbsp;</strong><strong>11.00 (am)</strong></p>
</td>
<td style="width: 13%;">
<p><strong>11.00 - 01.00 (pm)</strong></p>
</td>
<td style="width: 12.8571%;">
<p><strong>01.00 -&nbsp;</strong><strong>02.30 (pm)</strong></p>
</td>
<td style="width: 16.1429%;">
<p><strong>02.30&nbsp;</strong><strong>- 04.30 (pm)</strong></p>
</td>
<td style="width: 14%;">
<p><strong>04.30 - 05.00 (pm)</strong></p>
</td>
</tr>
<tr>
<td style="width: 10%;">
<p><strong>Day 1</strong></p>
</td>
<td style="width: 13%;">
<p>Topic 1: Digital Skill and Learning Platform</p>
<p>&nbsp;</p>
</td>
<td style="width: 13%;" rowspan="2">
<p>BREAK</p>
</td>
<td style="width: 13%;">
<p>Topic 1: Digital Skill and Learning Platform</p>
<p>&nbsp;</p>
</td>
<td style="width: 12.8571%;" rowspan="2">
<p>BREAK</p>
</td>
<td style="width: 16.1429%;">
<p>Topic 2: Augmented Reality (AR)</p>
<p>&nbsp;</p>
</td>
<td style="width: 14%;" rowspan="2">
<p>BREAK</p>
</td>
</tr>

<tr>
<td style="width: 10%;">
<p><strong>Day 2</strong></p>
</td>
<td style="width: 13%;">
<p>Topic 3: Virtual Reality (VR) and 360 degree</p>
<p>&nbsp;</p>
</td>

<td style="width: 13%;">
<p>Topic 4: Developing Engaging and Interactive Content</p>
<p>&nbsp;</p>
</td>

<td style="width: 16.1429%;">
<p>Topic 4: Developing Engaging and Interactive Content</p>
<p>&nbsp;</p>
</td>

</tr>
</tbody>
</table>
               
            </div>



          </div>


      </div>
      
  </div>

  <?php 
  if( isset($_SESSION['nama']) )  { 
      ?>
  <script>
  function actionEnroll()
  {
    let tdate = document.getElementById("tdate").value;
    let tdateText = $("#tdate option:selected").text();
    let token = document.getElementById("token").value;
    let course = 1;
        Swal.fire({
                title: 'Are you sure to Enroll This Course?',
                text: ""+tdateText,
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Enroll Now!',
                preConfirm: (login) => {
                    // return fetch(`//api.github.com/users/${login}`)
                    return fetch(`dashboard/actionenroll.php?session=`+tdate+`&course=`+course+`&token=`+token)
                    .then(response => {
                        if (!response.status) {
                        throw new Error(response.messages)
                        }
                        return response.json()
                    })
                    .catch(error => {
                        Swal.showValidationMessage(
                        `Request failed: ${messages}`
                        )
                    })
                },
                allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    console.log(result);
                if (result.isConfirmed || result.value) {
                    Swal.fire(
                    'Enrolled!',
                    'Thank you for enrolling for this course.<br>You can view the enrollment status in your dashboard.',
                    'success'
                    );
                    // setTimeout(function(){ window.location='admin'; }, 2000);

                }
                });
  }
  </script>
  <?php } 
  else
  { 
?>
<script>
  function actionEnroll()
  {


    Swal.fire({
                title: '<strong>User not found!</strong>',
                icon: 'info',
                html:
                    'You are not login / register to DaTA yet. Please login / register, ' + '',
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonText:
                    '<i class="fa fa-lock"></i> Login!',
                confirmButtonAriaLabel: 'Login!',
                cancelButtonText:
                    '<i class="fa fa-users"></i> Register',
                cancelButtonAriaLabel: 'Register!'
                }).then((result) => {
                    console.log(result);
                if (result.isConfirmed || result.value) {
                    window.location='login';
                }
                else if(result.dismiss == "cancel")
                {
                    window.location='register';
                }
                });

  }
  </script>
  <?php } ?>