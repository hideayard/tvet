<script>
        $(function(){
            var form = $(".login-form");

            form.css({
                opacity: 1,
                "-webkit-transform": "scale(1)",
                "transform": "scale(1)",
                "-webkit-transition": ".5s",
                "transition": ".5s"
            });

            /* validation */
     $("#login_form").validate({
      rules:
      {
            password: {
            required: true,
            },
            username: {
            required: true,
            //email: true
            },
       },
       messages:
       {
            password:{
                      required: "please enter your password"
                     },
            username: "please enter your username address",
       },
       submitHandler: submitForm    
       });  
       /* validation */
       
       /* login submit */
       function submitForm()
       {        
            var data = $("#login_form").serialize();
                
            $.ajax({
                
            type : 'POST',
            url  : 'dashboard/actionlogin.php',
            data : data,
            beforeSend: function()
            {   
                $("#error").fadeOut();
                $("#btn-login").html('<i class="fa fa-sync fa-spin"></i> &nbsp; Signing In');
            },
            success :  function(response)
               {          
                 try{
                        rv = JSON.parse(response);
                        if(isEmpty(rv) || rv.status==false)
                        {
                            console.log("NO DATA : ", response);
                            $("#error").fadeIn(500, function(){                        
                            $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Username Or Password is Wrong !</div>');
                            $("#btn-login").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Sign In');
                                    });
                        }
                        else
                        {
                          if(rv.status==true)
                          {
                            $("#error").fadeIn(500, function(){   
                                $("#error").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Login Success !</div>');
                                $("#btn-login").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Sign In');
                            });
                            console.log("SUCCESS : ", rv);
                            setCookie("token", rv.info, 300);
                            setCookie("user", rv.user, 300);
                            setTimeout(function(){ window.location="index.php?token="+rv.info; }, 1000);
                            // window.location="index.php";
                            
                          }

                        }
                 }   
                 catch (e) {
                   
                  $("#error").fadeIn(500, function(){                        
                      $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Error :  '+e+' !</div>');
                      $("#btn-login").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Sign In');
                              });
                  
                console.log("ERROR : ", e);

                 }           
                    
              }
            });
                return false;
        }
       /* login submit */
        });

        
    </script>