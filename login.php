<div class="site-section">
        <div class="container">
        <div style="text-align:center;" id="error"><!-- error will be shown here ! --></div>
        <form class="form-signin" id="login_form" >
            <div class="row justify-content-center">
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="username">Username</label>
                            <input type="text" name="username" id="username"  class="form-control form-control-lg">
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="password">Password</label>
                            <input  name="password" id="password" type="password" class="form-control form-control-lg">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                        <button type="submit" name="btn-login" id="btn-login" class="btn btn-primary btn-block btn-flat">Sign In</button>

                            <!-- <input type="submit" name="btn-login" id="btn-login"  value="Log In" class="btn btn-primary btn-lg px-5"> -->
                        </div>
                    </div>
                </div>
            </div>
            
        </form>
          
        </div>
    </div>

    