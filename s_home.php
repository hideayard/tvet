<script>
        $(function(){
            var form = $("#subscribe_form");

            form.css({
                opacity: 1,
                "-webkit-transform": "scale(1)",
                "transform": "scale(1)",
                "-webkit-transition": ".5s",
                "transition": ".5s"
            });

            /* validation */
     $("#subscribe_form").validate({
      rules:
      {
            user_mail: {
            required: true,
            email: true
            },
       },
       messages:
       {
            user_mail: "please enter your email address",
       },
       submitHandler: submitForm    
       });  
       /* validation */
       
       /* login submit */
       function submitForm()
       {        
            var data = $("#subscribe_form").serialize();
                
            $.ajax({
                
            type : 'POST',
            url  : 'dashboard/actionsubscribe.php',
            data : data,
            beforeSend: function()
            {   
                $("#error").fadeOut();
                $("#btn-subs").html('<i class="fa fa-sync fa-spin"></i> &nbsp; Processing.');
            },
            success :  function(response)
               {          
                 try{
                        rv = JSON.parse(response);
                        if(isEmpty(rv) || rv.status==false)
                        {
                            console.log("NO DATA : ", response);
                            $("#error").fadeIn(500, function(){                        
                            $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Email is Already Exist or Wrong !</div>');
                            $("#btn-subs").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Subscribe');
                                    });
                        }
                        else
                        {
                          if(rv.status==true)
                          {
                            $("#error").fadeIn(500, function(){   
                                $("#error").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Subscribe Success !</div>');
                                $("#btn-subs").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Subscribe');
                            });
                            console.log("SUCCESS : ", rv);
                            
                          }

                        }
                 }   
                 catch (e) {
                   
                  $("#error").fadeIn(500, function(){                        
                      $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Error :  '+e+' !</div>');
                      $("#btn-subs").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Subscribe');
                              });
                  
                console.log("ERROR : ", e);

                 }           
                    
              }
            });
                return false;
        }
       /* login submit */
        });

        
    </script>